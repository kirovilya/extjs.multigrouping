Ext.Loader.setConfig({
    enabled: true,
    disableCaching: false
});
//Ext.Loader.setPath('Ext.ux', 'ux');

Ext.require([
    'Ext.grid.*',
    'Ext.layout.container.Border',
    'Ext.data.*',
    'Ext.form.field.Number',
    'Ext.form.field.Date',
    'Ext.tip.QuickTipManager',
    //'Ext.ux.DataTip',
    'BarsUp.grid.multigrouping.GroupingPanel',
    'BarsUp.grid.multigrouping.MultiGroupingSummary',
    'BarsUp.grid.multigrouping.MultigroupToExcel',
    'BarsUp.grid.MultiSearch',
    'BarsUp.grid.ColumnValuesField'
]);

Ext.define('Task', {
    extend: 'Ext.data.Model',
    idProperty: 'taskId',
    fields: [
        {name: 'projectId', type: 'int'},
        {name: 'project', type: 'string'},
        {name: 'taskId', type: 'int'},
        {name: 'description', type: 'string'},
        {name: 'estimate', type: 'float'},
        {name: 'rate', type: 'float'},
        {name: 'due', type: 'date', dateFormat:'m/d/Y'},
        {name: 'test'}
    ]
});

Ext.define('StageProfit', {
    extend: 'Ext.data.Model',
    idProperty: 'Id',
    fields: [
        {name: 'Id', type: 'string'},
        {name: 'id', type: 'string'},
        {name: 'otv_dep', type: 'string'},
        {name: 'statusdokumenta', type: 'string'},
        {name: 'dpcdate', type: 'date', dateFormat:'d.m.Y'},
        {name: 'pay_date', type: 'date', dateFormat:'d.m.Y'},
        {name: 'contract_partner_name', type: 'string'},
        {name: 'dateperedachi', type: 'date', dateFormat:'d.m.Y'},
        {name: 'contract_docnum', type: 'string'},
        {name: 'urik', type: 'string'},
        {name: 'contract_subektrf_name', type: 'string'},
        {name: 'bc_name', type: 'string'},
        {name: 'status', type: 'string'},
        {name: 'pay_amount', type: 'float'},
        {name: 'days_close', type: 'string'},
        {name: 'dateispolneniya', type: 'date', dateFormat:'d.m.Y'},
        {name: 'preseil_id', type: 'string'},
     	{name: 'kommentariikb', type: 'string'},
     	{name: 'wamount', type: 'float'},
        {name: 'preseil_name', type: 'string'},
     	{name: 'debt', type: 'float'},
        {name: 'contract_id', type: 'string'},
     	{name: 'kommentarii', type: 'string'},
     	{name: 'gruz_date', type: 'date', dateFormat:'d.m.Y'},
        {name: 'name', type: 'string'},
     	{name: 'proekt_code', type: 'string'},
     	{name: 'realnayadateakta', type: 'date', dateFormat:'d.m.Y'},
        {name: 'contract_subektrf_id', type: 'string'},
     	{name: 'amount', type: 'float'},
        {name: 'bc_id', type: 'string'},
     	{name: 'contract_dpcdate', type: 'date', dateFormat:'d.m.Y'}
    ]
});


var dataBase = [
    {projectId: 100, project: 'Ext Forms: Field Anchoring', taskId: 112, description: 'Integrate 2.0 Forms with 2.0 Layouts', estimate: 6, rate: 150, due:'06/24/2007', test: {id: 16, display: '11'}},
    {projectId: 100, project: 'Ext Forms: Field Anchoring', taskId: 113, description: 'Implement AnchorLayout', estimate: 4, rate: 150, due:'06/25/2007', test: {id: 15, display: '22'}},
    {projectId: 100, project: 'Ext Forms: Field Anchoring', taskId: 114, description: 'Add support for multiple<br>types of anchors', estimate: 8, rate: 150, due:'06/27/2007', test: {id: 14, display: '33'}},
    {projectId: 100, project: 'Ext Forms: Field Anchoring', taskId: 115, description: 'Testing and debugging', estimate: 8, rate: 0, due:'06/29/2007', test: {id: 13, display: '44'}},
    {projectId: 101, project: 'Ext Grid: Single-level Grouping', taskId: 101, description: 'Add required rendering "hooks" to GridView', estimate: 6, rate: 100, due:'07/01/2007', test: {id: 12, display: '55'}},
    {projectId: 101, project: 'Ext Grid: Single-level Grouping', taskId: 102, description: 'Extend GridView and override rendering functions', estimate: 6, rate: 100, due:'07/03/2007', test: {id: 11, display: '66'}},
    {projectId: 101, project: 'Ext Grid: Single-level Grouping', taskId: 103, description: 'Extend Store with grouping functionality', estimate: 4, rate: 100, due:'07/04/2007', test: {id: 10, display: '77'}},
    {projectId: 101, project: 'Ext Grid: Single-level Grouping', taskId: 121, description: 'Default CSS Styling', estimate: 2, rate: 100, due:'07/05/2007', test: {id: 9, display: '88'}},
    {projectId: 101, project: 'Ext Grid: Single-level Grouping', taskId: 104, description: 'Testing and debugging', estimate: 6, rate: 100, due:'07/06/2007', test: {id: 8, display: '99'}},
    {projectId: 102, project: 'Ext Grid: Summary Rows', taskId: 105, description: 'Ext Grid plugin integration', estimate: 4, rate: 125, due:'07/01/2007', test: {id: 7, display: '1010'}},
    {projectId: 102, project: 'Ext Grid: Summary Rows', taskId: 106, description: 'Summary creation during rendering phase', estimate: 4, rate: 125, due:'07/02/2007', test: {id: 6, display: '1111'}},
    {projectId: 102, project: 'Ext Grid: Summary Rows', taskId: 107, description: 'Dynamic summary updates in editor grids', estimate: 6, rate: 125, due:'07/05/2007', test: {id: 5, display: '1212'}},
    {projectId: 102, project: 'Ext Grid: Summary Rows', taskId: 108, description: 'Remote summary integration', estimate: 4, rate: 125, due:'07/05/2007', test: {id: 4, display: '1313'}},
    {projectId: 102, project: 'Ext Grid: Summary Rows', taskId: 109, description: 'Summary renderers and calculators', estimate: 4, rate: 125, due:'07/06/2007', test: {id: 3, display: '1414'}},
    {projectId: 102, project: 'Ext Grid: Summary Rows', taskId: 110, description: 'Integrate summaries with GroupingView', estimate: 10, rate: 125, due:'07/11/2007', test: {id: 2, display: '1515'}},
    {projectId: 102, project: 'Ext Grid: Summary Rows', taskId: 111, description: 'Testing and debugging', estimate: 8, rate: 125, due:'07/15/2007', test: {id: 1, display: '1616'}}
];
var data = [];

for (var i=0; i < 100; ++i) {
    var rec = Ext.clone(dataBase[Ext.Number.randomInt(0, 15)]);
 	//var rec = Ext.clone(dataBase[i]);
    rec.taskId = i+1;
    data.push(rec);
}

Ext.onReady(function(){
    
    Ext.tip.QuickTipManager.init();
    /*
    var store = Ext.create('Ext.data.Store', {
        model: 'Task',
        data: data,
        sorters: {property: 'due', direction: 'ASC'},
        groupers: [
            Ext.create('Ext.util.Grouper', {
                property: 'project',
                root: 'data'
            })
            ,
			Ext.create('Ext.util.Grouper', {
                property: 'estimate',
                root: 'data'
            })
        ]
    });
	*/
	
    var store = Ext.create('Ext.data.Store', {
        model: 'StageProfit',
        proxy: {
         	type: 'ajax',
         	url: '/static/data3.json',
         	reader: {
            	type: 'json',
            	rootProperty: 'stage_profit',
            	totalProperty  : 'totalCount',
			},
			noCache: false
     	},
     	autoLoad: true,
     	pageSize: 0
    });
    

   
    var grid = Ext.create('Ext.grid.Panel', {
        //width: 800,
        //height: 450,
        //frame: true,
        //title: 'Sponsored Projects',
        //iconCls: 'icon-grid',
        //renderTo: document.body,
        columnLines : true,
        store: store,
        //enableLocking: true,

        plugins: [
            {ptype:"gms",pluginId:"gms",filterOnEnter:false},
            //cellEditing,
            //,{
            //    ptype: 'datatip',
            //    tpl: 'Click to edit {description}'
            //}
        ],
        listeners: {
            beforeshowtip: function(grid, tip, data) {
                var cellNode = tip.triggerEvent.getTarget(tip.view.getCellSelector());
                if (cellNode) {
                    data.colName = tip.view.headerCt.columnManager.getHeaderAtIndex(cellNode.cellIndex).text;
                }
            },
            groupchanging: function(grid, groupers) {
            	Ext.each(groupers, function(grouper){
            		if (grouper.getProperty() == 'test'){
            			grouper.setGroupFn(function(item){
            				var root = this._root;
        					return (root ? item[root] : item)[this._property]['display'];
            			});
            		}
            	});
            }
        },
        selModel: {
            selType: 'cellmodel'
        },
        dockedItems: [{
            dock: 'top',
            xtype: 'toolbar',
            items: [{
                tooltip: 'Toggle the visibility of the summary row',
                text: 'Показывать итоги',
                enableToggle: true,
                pressed: true,
                handler: function() {
                    if (grid.lockedGrid) {
                        grid.lockedGrid.getView().getFeature('group').toggleSummaryRow();
                    } else {
                        grid.getView().getFeature('group').toggleSummaryRow();
                    }
                }
            }, {
                text: 'Экспорт',
                handler: function (){
                    var exporter = Ext.create('BarsUp.grid.multigrouping.MultigroupToExcel', {fileName: 'test.xlsx'});
                    exporter.start(grid);
                }
            }, {
                text: 'Развернуть всё',
                handler: function (){
                    grid.getView().getFeature('group').expandAll();
                }
            }, {
                text: 'Свернуть всё',
                handler: function (){
                    grid.getView().getFeature('group').collapseAll();
                }
            }]
        }],
        features: [
	        Ext.create('BarsUp.grid.multigrouping.MultiGroupingSummary', {
	            id: 'group',
	            ftype: 'groupingsummary',
	            groupHeaderTpl: '[{itemsCount}] {columnName}: {name}',
	            //hideGroupedHeader: true,
	            enableGroupingMenu: true,
	            startCollapsed: true,
	            showSummaryRowOnDetails: false
	        }), {
	            ftype: 'summary',
	            dock: 'bottom'
	        }
        ],
        //split: true,
        /*lockedGridConfig: {
            header: false,
            collapsible: true,
            width: 300,
            forceFit: true
        },
        lockedViewConfig: {
            scroll: 'horizontal'
        },*/
        
        columns: [
        	//new Ext.grid.RowNumberer({locked: true, width: 30}),
        	{
        		header: 'Ответственный департамент',
        		dataIndex: 'otv_dep',
        		summaryType: 'count'
        	},
        	{
        		header: 'Статус документа',
        		dataIndex: 'statusdokumenta',
        		filterField: {
        			xtype: 'columnvaluesfield'
        		}
        	},
        	{
        		header:'даты',
        		columns:[
	        	{
	        		header: 'Дата документа',
	        		dataIndex: 'dpcdate',
	        		xtype: 'datecolumn',
	        		format:'d.m.Y'
	        	},
	        	{
	        		header: 'Дата оплаты',
	        		dataIndex: 'pay_date',
	        		xtype: 'datecolumn',
	        		format:'d.m.Y'
	        	}]
	        },
        	{
        		header: 'Партнер',
        		dataIndex: 'contract_partner_name',
                filterField: {
        			xtype: 'columnvaluesfield'
        		}
        	},
        	{
        		header: 'Дата передачи',
        		dataIndex: 'dateperedachi',
        		xtype: 'datecolumn',
        		format:'d.m.Y'
        	},
        	{
        		header: 'Номер договора',
        		dataIndex: 'contract_docnum',
                filterField:true
        	},
        	{
        		header: 'Юридическое лицо',
        		dataIndex: 'urik',
                filterField: {
        			xtype: 'columnvaluesfield'
        		}
        	},
        	{
        		header: 'Субъект',
        		dataIndex: 'contract_subektrf_name',
                filterField:true
        	},
        	{
        		header: 'БЦ',
        		dataIndex: 'bc_name',
                filterField: {
        			xtype: 'columnvaluesfield'
        		}
        	},
        	{
        		header: 'Статус',
        		dataIndex: 'status',
                filterField: {
        			xtype: 'columnvaluesfield'
        		}
        	},
        	{
        		header:'суммы',
        		columns:[
	        	{
	        		header: 'Сумма оплаты',
	        		dataIndex: 'pay_amount',
	        		summaryType: 'sum',
	        		xtype: 'numbercolumn', 
	        		align:'right', 
					style: 'text-align:left',
					summaryRenderer: Ext.util.Format.numberRenderer('0,000.00'),
	                filterField:true
	        	},
	        	{
	        		header: 'Дней до закрытия',
	        		dataIndex: 'days_close',
	        		filterField: {
        				xtype: 'columnvaluesfield'
        			}
	        	},
	        	{
	        		header: 'Дата исполнения',
	        		dataIndex: 'dateispolneniya',
	        		xtype: 'datecolumn',
	        		format:'d.m.Y',
	                filterField:"datefield"
	        	},
	        	{
	        		header: 'Сумма W',
	        		dataIndex: 'wamount',
	        		summaryType: 'sum',
	        		xtype: 'numbercolumn', 
	        		align:'right', 
					style: 'text-align:left',
					summaryRenderer: Ext.util.Format.numberRenderer('0,000.00'),
	                filterField:true
	        	}
	        ]},
        	{
        		header: 'Сделка',
        		dataIndex: 'preseil_name',
                filterField:true
        	},
        	{
        		header: 'Долг',
        		dataIndex: 'debt',
        		summaryType: 'sum',
        		xtype: 'numbercolumn', 
        		align:'right', 
				style: 'text-align:left',
				summaryRenderer: Ext.util.Format.numberRenderer('0,000.00'),
                filterField:true
        	},
        	{
        		header: 'Дата отгрузки',
        		dataIndex: 'gruz_date',
        		xtype: 'datecolumn',
        		format:'d.m.Y',
                filterField:true
        	},
        	{
        		header: 'Наименование',
        		dataIndex: 'name',
                filterField:true
        	},
        	{
        		header: 'Сумма',
        		dataIndex: 'amount',
        		summaryType: 'sum',
        		xtype: 'numbercolumn', 
        		align:'right', 
				style: 'text-align:left',
				summaryRenderer: Ext.util.Format.numberRenderer('0,000.00'),
                filterField:true
        	},
        	{
        		header: 'Дата акта',
        		dataIndex: 'realnayadateakta',
        		xtype: 'datecolumn',
        		format:'d.m.Y',
                filterField:"datefield"
        	},
        	{
        		header: 'Дата договора',
        		dataIndex: 'contract_dpcdate',
        		xtype: 'datecolumn',
        		format:'d.m.Y',
                filterField:"datefield"
        	}
        ]
        /*
        columns: [{
            text: 'Task',
            flex: 1,
            //locked: true,
            tdCls: 'task',
            sortable: true,
            dataIndex: 'description',

            // This may have wrapped HTML which causes unpredictable row heights
            variableRowHeight: true,
            hideable: false,
            summaryType: 'count',
            summaryRenderer: function(value, summaryData, dataIndex) {
                return ((value === 0 || value > 1) ? '(' + value + ' Tasks)' : '(1 Task)');
            },
            field: {
                xtype: 'textfield'
            }
        }, {
            header: 'Project',
            width: 180,
            sortable: true,
            dataIndex: 'project'
        }, {
            header: 'Schedule',
            columns: [{
                header: 'Due Date',
                width: 125,
                sortable: true,
                dataIndex: 'due',
                summaryType: 'max',
                renderer: Ext.util.Format.dateRenderer('m/d/Y'),
                summaryRenderer: Ext.util.Format.dateRenderer('m/d/Y'),
                field: {
                    xtype: 'datefield'
                }
            }, {
                header: 'Estimate',
                width: 125,
                sortable: true,
                dataIndex: 'estimate',
                summaryType: 'sum',
                renderer: function(value, metaData, record, rowIdx, colIdx, store, view){
                    return value + ' hours';
                },
                summaryRenderer: function(value, summaryData, dataIndex) {
                    return value + ' hours';
                },
                field: {
                    xtype: 'numberfield'
                }
            }, {
                header: 'Rate',
                width: 125,
                sortable: true,
                renderer: Ext.util.Format.usMoney,
                summaryRenderer: Ext.util.Format.usMoney,
                dataIndex: 'rate',
                summaryType: 'average',
                field: {
                    xtype: 'numberfield'
                }
            }, {
                header: 'Cost',
                width: 114,
                sortable: false,
                groupable: false,
                renderer: function(value, metaData, record, rowIdx, colIdx, store, view) {
                    return Ext.util.Format.usMoney(record.get('estimate') * record.get('rate'));
                },
                summaryType: function(records, values) {
                    var i = 0,
                        length = records.length,
                        total = 0,
                        record;

                    for (; i < length; ++i) {
                        record = records[i];
                        total += record.get('estimate') * record.get('rate');
                    }
                    return total;
                },
                summaryRenderer: Ext.util.Format.usMoney
            }, {
                header: 'test',
                width: 40,
                sortable: true,
                dataIndex: 'test',
                renderer: function(value, metaData, record, rowIdx, colIdx, store, view){
                    return value['display'];
                }
            }]
        }]*/
    });
    var win = Ext.create('Ext.window.Window', {
        width: 800,
        height: 450,
        frame: true,
        maximized: true,
        title: 'Sponsored Projects',
        renderTo: document.body,
        layout: 'fit',
        items: [grid]
    });

    win.show();
});