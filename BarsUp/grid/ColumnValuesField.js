Ext.define('BarsUp.grid.ColumnValuesField', {
    extend: 'Ext.form.field.Picker',
    alias: 'widget.columnvaluesfield',

    matchFieldWidth: false,

    // grid store
    gridstore: null,

    // picker store
    store: null,

    // функция получения значения из записи грида
    getColValueFn: null,

    picker: null,

    config: {
        //editable: false
    },

    initComponent: function() {
        var me = this;

        me.callParent();

        me.store = Ext.create('Ext.data.Store', {
            fields: ['id', 'name']
        });
        // навесим обработчики чтобы узнать значения колонки грида
        if (me.gridstore) {
            me.gridstore.on(
                'load', me.loadRecords, me
            );
        }
        me.on('collapse', me.onSelect, me);
    },

    getColumnValue: function (record) {
        var me = this;
        if (me.getColValueFn && Ext.isFunction(me.getColValueFn)) {
            return me.getColValueFn(record);
        } else
            return record.get(me.itemId);
    },

    loadRecords: function(store, records) {
        var me = this,
            length = store.getCount(),
            record, value,
            values = new Array(),
            keys = new Array();
        for (var i = 0; i < length; i++) {
            record = records[i];
            value = me.getColumnValue(record);
            if (keys.indexOf(value) == -1) {
                keys.push(value);
                values.push({"id": value, "name": value});
            }
        }
        me.store.loadData(values);
    },

    createPicker: function() {
        var me = this,
            picker;

        picker = Ext.create('Ext.grid.Panel', {
            minWidth: 250,
            height: 250,
            closeAction: 'hide',
            floating: true,
            baseCls: Ext.baseCSSPrefix + 'boundlist',
            shrinkWrapDock: 2,
            manageHeight: false,
            shadow: false,
            resizable: true,
            selModel: 'checkboxmodel',
            store: me.store,
            columns: [
                {
                    header: "Выделить все записи",
                    dataIndex: 'name',
                    flex: 1,
                    width: 200
                }
            ],
            buttons: [
                {
                    text: 'Применить',
                    listeners: {
                        click: me.onSelect,
                        scope: me
                    }
                },
                {
                    text: 'Очистить',
                    listeners: {
                        click: me.onClear,
                        scope: me
                    }
                }
            ],
            buttonAlign: 'center'
        });

        return picker;
    },

    onSelect: function(){
        var me = this,
            selected = me.picker.getView().getSelectionModel().getSelection(),
            values = "",
            val;
        for (var x = 0; x < selected.length; x++) 
        {
            values = x != 0 ? values + "|" : values;
            val = selected[x].data['name'];
            val = val == undefined ? "_-_" : val;
            values = values + val;
        }
        if (selected.length > 0) {
            me.setValue('in '+values);
            me.fireEvent('select', me, values);
        } else 
            me.setValue('');
        me.collapse();
    },

    onClear: function(){
        var me = this;
        me.picker.getView().getSelectionModel().deselectAll();
        me.setValue('');
        me.collapse();
    }
});