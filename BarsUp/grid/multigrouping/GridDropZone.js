Ext.define('BarsUp.grid.multigrouping.GridDropZone', {
    extend: 'Ext.dd.DropZone',
    colHeaderCls: Ext.baseCSSPrefix + 'column-header',
    proxyOffsets: [-4, -9],
    gridViewCls: Ext.baseCSSPrefix + 'grid-view',
    dropAllowed: Ext.baseCSSPrefix + 'grid-col-remove-icon',

    constructor: function(grid){
        this.grid = grid;
        this.ddGroup = this.getDDGroup();
        this.callParent([grid.getView().id]);
    },

    disable: function() {
        this.disabled = true;
    },
    
    enable: function() {
        this.disabled = false;
    },

    getDDGroup: function() {
        return 'header-dd-zone-' + this.grid.id;
    },

    getTargetFromEvent : function(e){
        return e.getTarget('.' + this.gridViewCls);
    },

    onNodeOver: function(node, dragZone, e, data) {
        var me = this,
            header = data.header;
        
        me.valid = header instanceof Ext.grid.column.Column;

        return me.valid ? me.dropAllowed : me.dropNotAllowed;
    },

    onNodeOut: function() {        
    },

    onNodeDrop: function(node, dragZone, e, data) {
    }
});