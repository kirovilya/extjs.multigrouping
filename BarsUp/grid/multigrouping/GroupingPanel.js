Ext.define('BarsUp.grid.multigrouping.GroupingPanel', {
    extend: 'Ext.AbstractPlugin',
    requires: [
        'BarsUp.grid.multigrouping.Container',
        'BarsUp.grid.multigrouping.Column',
        'BarsUp.grid.multigrouping.DragZone', 
        'BarsUp.grid.multigrouping.DropZone',
        'BarsUp.grid.multigrouping.GridDropZone'
    ],
    alias: 'plugin.groupingpanel',

    localeProperties: {
        groupingPanelText:      '',
        showGroupingPanelText:  '',
        hideGroupingPanelText:  '',
        clearGroupText:         ''
    },
    
    groupingPanelText:      'Перетащите колонку сюда чтобы сгруппировать', //'Drag a column header here to group by that column',
    showGroupingPanelText:  'Показать панель группировки', //'Show Group By Panel',
    hideGroupingPanelText:  'Скрыть панель группировки', //'Hide Group By Panel',
    clearGroupText:         'Очистить группы', //'Clear Group',
    groupingPanelIconCls:   Ext.baseCSSPrefix + 'group-by-panel-icon',
    clearGroupIconCls:      Ext.baseCSSPrefix + 'clear-icon',

    constructor: function(config){
        var me = this;
        
        if(config && config.cmp){
            me.listenersState = config.cmp.on({
                beforestatesave:    me.onStateSave,
                beforestaterestore: me.onStateRestore,
                scope:              me,
                destroyable:        true
            });
        }
        
        me.callParent(arguments);
    },
    
    init: function(grid) {
        var me = this;

        me.callParent(arguments);
        me.grid = grid;

        me.listenersRender = me.grid.on({
            beforerender: function(){
                me.groupingCt = me.grid.addDocked(Ext.create('BarsUp.grid.multigrouping.Container',{
                    panelPlugin: me
                }))[0];
            },
            afterrender: function(){
                me.dragZone = new BarsUp.grid.multigrouping.DragZone(me.groupingCt);
                me.dropZone = new BarsUp.grid.multigrouping.DropZone(me.groupingCt);
                me.gridDropZone = new BarsUp.grid.multigrouping.GridDropZone(me.grid);
                
                if(me.disabled === true){
                    me.disable();
                }else{
                    me.enable();
                }
            },
            single:         true,
            scope:          me,
            destroyable:    true
        });
        
        me.listenersConfig = me.grid.on({
            configurationapplied:   me.initGroupingColumns,
            getcontextmenuitems:    me.onContextMenu,
            scope:                  me,
            destroyable:            true
            
        });
        me.grid.getStore().on({
        	groupchange: me.initGroupingColumns,
        	scope: me
        });
    },

    destroy: function() {
        var me = this;

        Ext.destroy(me.listenersState, me.listenersRender, me.listenersConfig, me.groupingCt, me.dragZone, me.dropZone);
        
        me.callParent(arguments);
    },
    
    enable: function() {
        var me = this;

        me.disabled = false;
        if (me.dropZone) {
            me.dropZone.enable();
        }
        if (me.dragZone) {
            me.dragZone.enable();
        }
        
        if(me.groupingCt){
            me.groupingCt.show();
            me.initGroupingColumns();
        }
    },
    
    disable: function() {
        var me = this;

        me.disabled = true;
        if (me.dropZone) {
            me.dropZone.disable();
        }
        if (me.dragZone) {
            me.dragZone.disable();
        }
        if(me.groupingCt){
            me.groupingCt.hide();
        }
    },
    
    onGroupsChanged: function(ct, groupers){
        var me = this;
        
        if(me.disabled) {
            return;
        }
        
        var store = me.grid.getStore();

        me.grid.fireEvent('groupchanging', me.grid, groupers);
        store.suspendEvents();
        store.groupers = groupers;
        store.resumeEvents();
        store.fireEvent('refresh', store);
        me.grid.fireEvent('groupchange', me.grid, groupers);
    },
    
    onGroupSort: function(column, direction){
        var me = this,
            store = me.grid.getStore();
        
        if(me.disabled) {
            return false;
        }
        
        Ext.Array.each(store.groupers, function(item, index, len){
            if(item.getProperty() == column.dataIndex){
                item.setDirection(direction);
            }
        });

        store.sort(column.dataIndex, direction);
    },
    
    initGroupingColumns: function(){
        var me = this,
            groupers = me.getGroupers(),
            columns = Ext.Array.toValueMap( me.grid.headerCt.getGridColumns(), 'dataIndex'),
            columnsByDisplay = Ext.Array.toValueMap( me.grid.headerCt.getGridColumns(), 'displayField'),
            newCol;
        
        me.groupingCt.removeAll();
        
        Ext.suspendLayouts();

        Ext.Array.each(groupers, function(item, index, len){
            var col = columnsByDisplay[item.getProperty()] || columns[item.getProperty()];
            
            if(col){
                me.groupingCt.addColumn({
                    text:       col.text,
                    idColumn:   col.id,
                    dataIndex:  item.getProperty(),
                    direction:  item.getDirection()
                }, -1);
            }
            
        });
        
        me.groupingCt.buildTreeColumns();

        Ext.resumeLayouts(true);
    },
    
    showHideGroupingPanel: function(){
        var me = this;
        
        if(me.disabled){
            me.enable();
        }else{
            me.disable();
        }
    },
    
    onContextMenu: function(grid, contextType, items, config){
        if(contextType != 'header') {
            return;
        }
        Ext.Array.insert(items, 0, this.getContextMenu());
    },
    
    getContextMenu: function(){
        var me = this, items = [];
        
        items.push({
            iconCls: me.groupingPanelIconCls,
            text: me.disabled ? me.showGroupingPanelText : me.hideGroupingPanelText,
            handler: function(){
                if(me.disabled){
                    me.enable();
                    me.grid.fireEvent('showgroupingpanel', me);
                }else{
                    me.disable();
                    me.grid.fireEvent('hidegroupingpanel', me);
                }
            }
        });
        
        if(me.getGroupers() && me.getGroupers().length > 0){
            items.push({
                iconCls: me.clearGroupIconCls,
                text: me.clearGroupText,
                handler: function(){
                    me.grid.getStore().clearGrouping();
                    me.initGroupingColumns();
                }
            });
        }
        
        return items;
    },
    
    onStateSave: function(cmp, state, eOpts){
        var me = this;

        state['showGroupingPanel'] = !me.disabled;
    },
    
    onStateRestore: function(cmp, state, eOpts){
        var me = this;

        if(state['showGroupingPanel'] === true){
            me.enable();
        }else{
            me.disable();
        }
    },

    getGroupers: function(cmp) {
    	var me = this,
    		grouper = me.grid.getStore().getGrouper();
    	if (grouper) {
    		return [grouper];
    	} else {
    		return grouper = me.grid.getStore().groupers;
    	}
    }
});