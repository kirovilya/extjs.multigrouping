Ext.define('BarsUp.grid.multigrouping.MultiGrouping', {
    extend: 'Ext.grid.feature.Feature',
    mixins: {
        summary: 'Ext.grid.feature.AbstractSummary'
    },
    requires: [
        'BarsUp.grid.multigrouping.MultiGroupingStore',
        'BarsUp.grid.multigrouping.GroupingPanel'
    ],

    alias: 'feature.multigrouping',

    eventPrefix: 'group',
    eventSelector: '.' + Ext.baseCSSPrefix + 'grid-group-hd',
    summaryRowSelector: '.' + Ext.baseCSSPrefix + 'grid-row-summary',

    refreshData: {},
    wrapsItem: true,

    groupHeaderTpl: '{columnName}: {name}',

    depthToIndent: 17,

    collapsedCls: Ext.baseCSSPrefix + 'grid-group-collapsed',
    hdCollapsedCls: Ext.baseCSSPrefix + 'grid-group-hd-collapsed',
    hdNotCollapsibleCls: Ext.baseCSSPrefix + 'grid-group-hd-not-collapsible',
    collapsibleCls: Ext.baseCSSPrefix + 'grid-group-hd-collapsible',
    ctCls: Ext.baseCSSPrefix  + 'group-hd-container',

    groupByText : 'Сгруппировать по полю', //'Group by this field',

    showGroupsText : 'Show in groups',

    hideGroupedHeader : false,

    startCollapsed : false,

    enableGroupingMenu : true,

    enableNoGroups : false,

    collapsible: true,

    expandTip: 'Click to expand. CTRL key collapses all others',

    collapseTip: 'Click to collapse. CTRL/click collapses all others',

    showSummaryRow: false,
    showSummaryRowOnDetails: true,

    outerTpl: [
        '{%',
            'if (!(this.groupingFeature.disabled || values.rows.length === 1 && values.rows[0].isSummary)) {',
                'this.groupingFeature.setup(values.rows, values.view.rowValues);',
            '}',

            'this.nextTpl.applyOut(values, out, parent);',

            'if (!(this.groupingFeature.disabled || values.rows.length === 1 && values.rows[0].isSummary)) {',
                'this.groupingFeature.cleanup(values.rows, values.view.rowValues);',
            '}',
        '%}',
    {
        priority: 200
    }],

    groupRowTpl: [
        '{%',
            'var me = this.groupingFeature,',
                'colspan = "colspan=" + values.columns.length;',
            'if (me.disabled || parent.rows.length === 1 && parent.rows[0].isSummary) {',
                'values.needsWrap = false;',
            '} else {',
                'me.setupRowData(values.record, values.rowIndex, values);',
            '}',
        '%}',
        '<tpl if="needsWrap">',
            '{%',
                'var topParent = parent;\n',
            '%}',
            '{% values.view.renderColumnSizer(values, out); %}',
                '<tpl for="groupInfo">',                    
                    //'{% debugger; %}',
                    '<tr data-boundView="{parent.view.id}" data-recordId="{parent.record.internalId:htmlEncode}" data-recordIndex="{[values.isCollapsedGroup ? -1 : parent.recordIndex]}" class="{groupHeaderCls}">',
                        '<td class="{[me.ctCls]}" {[colspan]}>',
                            '{%',
                                'var groupTitleStyle = (!parent.view.lockingPartner || (parent.view.ownerCt === parent.view.ownerCt.ownerLockable.lockedGrid) || (parent.view.lockingPartner.headerCt.getVisibleGridColumns().length === 0)) ? "" : "visibility:hidden";',
                                'me.lastDepth = values.depth;\n',
                                'groupTitleStyle = groupTitleStyle + ";margin-left: " + values.depth + "px;";\n',
                            '%}',
                            '<div data-groupname="{groupId:htmlEncode}" class="', Ext.baseCSSPrefix, 'grid-group-hd {collapsibleCls}" nottabindex="0" hidefocus="on" {ariaCellInnerAttr}>',
                                '<div class="', Ext.baseCSSPrefix, 'grid-group-title" style="{[groupTitleStyle]}" {ariaGroupTitleAttr}>',
                                    '{[parent.groupHeaderTpl.apply(values, topParent) || "&#160;"]}',
                                '</div>',
                            '</div>',
                        '</td>',
                    '</tr>',
                '</tpl>',
            '<tpl if="!isCollapsedGroup">',
                '{%',
                    'values.itemClasses.length = 0;',
                    'this.nextTpl.applyOut(values, out, parent);',
                '%}',
            '</tpl>',
            '<tpl for="summaryRecords">',
                '{%me.outputSummaryRecord(values, parent, out, topParent);%}',
            '</tpl>',

        '<tpl else>',
            '{%this.nextTpl.applyOut(values, out, parent);%}',
        '</tpl>', {
            priority: 200,

            beginRowSync: function (rowSync) {
                var owner = this.owner;

                rowSync.add('header', owner.eventSelector);
                rowSync.add('summary', owner.summaryRowSelector);
            },

            syncContent: function(destRow, sourceRow, columnsToUpdate) {
                var i, len;
                destRow = Ext.fly(destRow, 'syncDest');
                sourceRow = Ext.fly(sourceRow, 'sycSrc');
                var owner = this.owner,
                    destHds = destRow.query(owner.eventSelector, true),
                    sourceHds = sourceRow.query(owner.eventSelector, true),
                    destSummaryRows = destRow.query(owner.summaryRowSelector, true),
                    sourceSummaryRows = sourceRow.query(owner.summaryRowSelector, true);

                if (destHds && sourceHds) {
                    for (i = 0; i < destHds.length; i++) {
                        if (sourceHds[i]) {
                            Ext.fly(destHds[i]).syncContent(sourceHds[i]);
                        }
                    }
                }

                if (destSummaryRows && sourceSummaryRows) {

                    if (columnsToUpdate) {
                        for (i = 0; i < destSummaryRows.length; i++) {
                            if (sourceSummaryRows[i]) {
                                this.groupingFeature.view.updateColumns(destSummaryRows[i], sourceSummaryRows[i], columnsToUpdate);
                            }
                        }
                    }
                    else {
                        for (i = 0; i < destSummaryRows.length; i++) {
                            if (sourceSummaryRows[i]) {
                                Ext.fly(destSummaryRows[i]).syncContent(sourceSummaryRows[i]);
                            }
                        }
                    }
                }
            }
        }
    ],

    constructor: function() {
        this.groupCache = {};
        this.groupInfo = {};
        this.groupingpanel = Ext.create('BarsUp.grid.multigrouping.GroupingPanel');
        this.callParent(arguments);
    },

    init: function(grid) {
        var me = this,
            view = me.view,
            store = view.getStore(),
            lockPartner, dataSource;

        view.isGrouping = !!me.getGroupers();
        view.hasActiveFeature = me.hasActiveFeature;
        // нужно для корректного подсчета
        view.variableRowHeight = true;
        view.onUpdate = Ext.bind(me.onUpdate, view);
        me.oldclearViewEl = view.clearViewEl;
        view.clearViewEl = Ext.bind(me.clearViewEl, me);

        if (me.lockingPartner && me.lockingPartner.groupCache) {
            me.groupCache = me.lockingPartner.groupCache;
        }

        me.mixins.summary.init.call(me);

        me.callParent(arguments);
        grid.on({
            columnhide: me.onColumnHideShow,
            columnshow: me.onColumnHideShow,
            columnmove: me.onColumnMove,
            scope: me
        });

        view.addTpl(Ext.XTemplate.getTpl(me, 'outerTpl')).groupingFeature = me;

        view.addRowTpl(Ext.XTemplate.getTpl(me, 'groupRowTpl')).groupingFeature = me;

        view.preserveScrollOnRefresh = true;

        if (store.isBufferedStore) {
            me.collapsible = false;
        }
        else {
            lockPartner = me.lockingPartner;
            if (lockPartner && lockPartner.dataSource) {
                me.dataSource = view.dataSource = dataSource = lockPartner.dataSource;
            } else {
                me.dataSource = view.dataSource = dataSource = new BarsUp.grid.multigrouping.MultiGroupingStore(me, store);
            }
        }
        this.groupingpanel.init(grid);

        grid = grid.ownerLockable || grid;

        grid.on({
            beforeReconfigure: me.beforeReconfigure,
            scope: me
        });

        view.on({
            afterrender: me.afterViewRender,
            scope: me,
            single: true
        });

        if (dataSource) {
            dataSource.on('groupchange', me.onGroupChange, me);
        } else {
            me.storeListeners = view.store.on({
                groupchange: me.onGroupChange,
                scope: me,
                destroyable: true
            });
        }
    },

    clearViewEl: function(leaveNodeContainer) {
    	var me = this, 
    		startIndex = me.view.all.startIndex;
    	me.oldclearViewEl.apply(me.view, arguments);
    	me.view.all.startIndex = startIndex;
    },

    indexOf: function(record) {
        return this.dataSource.indexOf(record);
    },

    isInCollapsedGroup: function(record) {
        var groupData,
            store = this.view.getStore();

        if (store.isGrouped() && (groupData = this.getGroup(record))) {
            return groupData.isCollapsed || false;
        }
        return false;
    },

    clearGroupCache: function() {
        var me = this,
            groupCache = me.groupCache = {};

        if (me.lockingPartner) {
            me.lockingPartner.groupCache = groupCache;
        }
        return groupCache;
    },

    vetoEvent: function(record, row, rowIndex, e) {
        if (e.type !== 'mouseover' && e.type !== 'mouseout'  && e.type !== 'mouseenter' && e.type !== 'mouseleave' && e.getTarget(this.eventSelector)) {
            return false;
        }
    },

    enable: function() {
        var me = this,
            view = me.view,
            store = view.getStore(),
            groupToggleMenuItem;

        view.isGrouping = true;
        if (view.lockingPartner) {
            view.lockingPartner.isGrouping = true;
        }
        me.callParent();
        if (me.lastGrouper) {
            store.group(me.lastGrouper);
            me.lastGrouper = null;
        }
        groupToggleMenuItem = me.view.headerCt.getMenu().down('#groupToggleMenuItem');
        if (groupToggleMenuItem) {
            groupToggleMenuItem.setChecked(true, true);
        }
    },

    disable: function() {
        var me = this,
            view = me.view,
            store = view.getStore(),
            groupToggleMenuItem,
            lastGrouper = store.getGrouper();

        view.isGrouping = false;
        if (view.lockingPartner) {
            view.lockingPartner.isGrouping = false;
        }
        me.callParent();
        if (lastGrouper) {
            me.lastGrouper = lastGrouper;
            store.clearGrouping();
        }

        groupToggleMenuItem = me.view.headerCt.getMenu().down('#groupToggleMenuItem');
        if (groupToggleMenuItem) {
            groupToggleMenuItem.setChecked(false, true);
        }
    },

    afterViewRender: function() {
        var me = this,
            view = me.view;

        view.on({
            scope: me,
            groupclick: me.onGroupClick
        });

        if (me.enableGroupingMenu) {
            me.injectGroupingMenu();
        }

        me.pruneGroupedHeader();

        me.lastGrouper = me.view.getStore().getGrouper();

        if (me.disabled) {
            me.disable();
        }

        view.on('scroll', me.onViewScroll, me);
    },

    injectGroupingMenu: function() {
        var me = this,
            headerCt = me.view.headerCt;

        headerCt.showMenuBy = me.showMenuBy;
        headerCt.getMenuItems = me.getMenuItems();
    },

    onColumnHideShow: function(headerOwnerCt, header) {
        var me = this,
            view = me.view,
            headerCt = view.headerCt,
            menu = headerCt.getMenu(),
            activeHeader = menu.activeHeader,
            groupMenuItem  = menu.down('#groupMenuItem'),
            groupMenuMeth,
            colCount = me.grid.getVisibleColumnManager().getColumns().length,
            items,
            len,
            i;

        if (activeHeader && groupMenuItem) {
            groupMenuMeth = activeHeader.groupable === false || !activeHeader.dataIndex || me.view.headerCt.getVisibleGridColumns().length < 2 ?  'disable' : 'enable';
            groupMenuItem[groupMenuMeth]();
        }

        if (view.rendered && colCount) {
            items = view.el.query('.' + me.ctCls);
            for (i = 0, len = items.length; i < len; ++i) {
                items[i].colSpan = colCount;
            }
        }
    },

    onColumnMove: function() {
        var me = this,
            store = me.view.store,
            groups,
            groupName,
            group, firstRec, lastRec;

        if (me.dataSource.isGrouped()) {
            groups = me.groupCache;
            Ext.suspendLayouts();
            for (groupName in groups) {
                if (groups.hasOwnProperty(groupName)) {
                    group = groups[groupName];
                    if (group.isCollapsed) {
                        store.fireEvent('update', store, group, 'edit', null);
                    } else {
                        firstRec = group.items[0];
                        lastRec = group.items[group.items.length - 1];

                        store.fireEvent('update', store, firstRec, 'edit', null);
                        if (lastRec !== firstRec && me.showSummaryRow) {
                            store.fireEvent('update', store, lastRec, 'edit', null);
                        }
                    }
                }
            }
            Ext.resumeLayouts(true);
        }
    },

    showMenuBy: function(clickEvent, t, header) {
        var me = this,
            menu = me.getMenu(),
            groupMenuItem = menu.down('#groupMenuItem'),
            groupMenuMeth = header.groupable === false || !header.dataIndex || me.view.headerCt.getVisibleGridColumns().length < 2 ?  'disable' : 'enable',
            groupToggleMenuItem  = menu.down('#groupToggleMenuItem'),
            isGrouped = false,
            groupers = me.grid.getStore().groupers,
            grouperslen = groupers ? groupers.length : 0,
            grouper, i;
        for (i = 0; i < grouperslen; i++) {
            grouper = groupers[i];
            if (grouper.getProperty() == header.dataIndex) {
                isGrouped = true;
            }
        }

        groupMenuItem[groupMenuMeth]();
        if (groupToggleMenuItem) {
            groupToggleMenuItem.setChecked(isGrouped, true);
            groupToggleMenuItem[isGrouped ?  'enable' : 'disable']();
        }
        Ext.grid.header.Container.prototype.showMenuBy.apply(me, arguments);
    },

    getMenuItems: function() {
        var me                 = this,
            groupByText        = me.groupByText,
            disabled           = me.disabled || !me.getGroupField(),
            showGroupsText     = me.showGroupsText,
            enableNoGroups     = me.enableNoGroups,
            getMenuItems       = me.view.headerCt.getMenuItems;

        return function() {
            var o = getMenuItems.call(this);
            o.push('-', {
                iconCls: Ext.baseCSSPrefix + 'group-by-icon',
                itemId: 'groupMenuItem',
                text: groupByText,
                handler: me.onGroupMenuItemClick,
                scope: me
            });
            if (enableNoGroups) {
                o.push({
                    itemId: 'groupToggleMenuItem',
                    text: showGroupsText,
                    checked: !disabled,
                    checkHandler: me.onGroupToggleMenuItemClick,
                    scope: me
                });
            }
            return o;
        };
    },

    onGroupMenuItemClick: function(menuItem, e) {
        var me = this,
            menu = menuItem.parentMenu,
            hdr  = menu.activeHeader,
            view = me.view,
            store = view.store,
            groupers = store.groupers,
            grouperslen = groupers ? groupers.length : 0,
            i, grouper, isGrouped = false;


        if (me.disabled) {
            me.lastGrouper = null;
            me.block();
            me.enable();
            me.unblock();
        }
        view.isGrouping = true;

        for (i = 0; i < grouperslen; i++) {
            grouper = groupers[i];
            if (grouper.getProperty() == hdr.dataIndex) {
                isGrouped = true;
                break;
            }
        }
        if (!isGrouped) {
            if (store.groupers) {
                store.groupers.push(Ext.create('Ext.util.Grouper', {
                    property: hdr.dataIndex,
                    root: 'data',
                    direction: hdr.direction
                }));
            } else {
                store.groupers = [Ext.create('Ext.util.Grouper', {
                    property: hdr.dataIndex,
                    root: 'data',
                    direction: hdr.direction
                })];
            }
        } else {
            if (grouper) {
                store.groupers.splice(i, 1);
            }
        }
        store.resumeEvents();
        me.grid.fireEvent('groupchanging', me.grid, store.groupers);
        store.fireEvent('refresh', store);
        store.fireEvent('groupchange', me, store.groupers);
        me.pruneGroupedHeader();
    },

    block: function(fromPartner) {
        this.blockRefresh = this.view.blockRefresh = true;
        if (this.lockingPartner && !fromPartner) {
            this.lockingPartner.block(true);
        }
    },

    unblock: function(fromPartner) {
        this.blockRefresh = this.view.blockRefresh = false;
        if (this.lockingPartner && !fromPartner) {
            this.lockingPartner.unblock(true);
        }
    },

    onGroupToggleMenuItemClick: function(menuItem, checked) {
        this[checked ? 'enable' : 'disable']();
    },

    pruneGroupedHeader: function() {
        var me = this,
            header = me.getGroupedHeader();

        if (me.hideGroupedHeader && header) {
            Ext.suspendLayouts();
            if (me.prunedHeader && me.prunedHeader !== header) {
                me.prunedHeader.show();
            }
            me.prunedHeader = header;
            if (header.rendered) {
                header.hide();
            }
            Ext.resumeLayouts(true);
        }
    },

    getHeaderNode: function(groupName) {
        var el = this.view.getEl(),
            nodes, i, len, result, node;


        if (el) {
            groupName = Ext.htmlEncode(groupName);
            nodes = el.query(this.eventSelector);
            for (i = 0, len = nodes.length; i < len; ++i) {
                node = nodes[i];
                if (node.getAttribute('data-groupName') === groupName) {
                    return node;
                }
            }
        }
    },

    getGroup: function(name) {
        if (name.isModel) {
            return this.getRecordGroup(name);
        }
        var cache = this.groupCache,
            item = cache[name];

        if (!item) {
            item = cache[name] = {
                isCollapsed: true
            };
        }
        return item;
    },

    isExpanded: function(groupName) {
        return !this.getGroup(groupName).isCollapsed;
    },

    expand: function(groupName, focus) {
        this.doCollapseExpand(false, groupName, focus);
    },

    expandcollapseGroup: function(group, isCollapsed) {
        var item, groupId;
        if (group instanceof Ext.util.Group) {
            groupId = this.getGroupKey(group);
            this.groupCache[groupId] = group;
            group.isCollapsed = isCollapsed;
            for (item in group.items) {
                this.expandcollapseGroup(group.getAt(item), isCollapsed);
            }
        }
    },

    expandAll: function() {
        var me = this,
            groupCache = me.groupCache,
            groupName,
            lockingPartner = me.lockingPartner;

        for (groupName in groupCache) {
            if (groupCache.hasOwnProperty(groupName)) {
                groupCache[groupName].isCollapsed = false;
                me.expandcollapseGroup(groupCache[groupName], false);
            }
        }
        Ext.suspendLayouts();
        me.dataSource.onRefresh();
        Ext.resumeLayouts(true);

        for (groupName in groupCache) {
            if (groupCache.hasOwnProperty(groupName)) {
                me.afterCollapseExpand(false, groupName);
                if (lockingPartner) {
                    lockingPartner.afterCollapseExpand(false, groupName);
                }
            }
        }
    },

    collapse: function(groupName, focus) {
        this.doCollapseExpand(true, groupName, focus);
    },

    isAllCollapsed: function() {
        var me = this,
            groupCache = me.groupCache,
            groupName;

        for (groupName in groupCache) {
            if (groupCache.hasOwnProperty(groupName)) {
                if (!groupCache[groupName].isCollapsed) {
                    return false;
                }
            }
        }
        return true;
    },

    isAllExpanded: function() {
        var me = this,
            groupCache = me.groupCache,
            groupName;

        for (groupName in groupCache) {
            if (groupCache.hasOwnProperty(groupName)) {
                if (groupCache[groupName].isCollapsed) {
                    return false;
                }
            }
        }
        return true;
    },

    collapseAll: function() {
        var me = this,
            groupCache = me.groupCache,
            groupName,
            lockingPartner = me.lockingPartner;

        for (groupName in groupCache) {
            if (groupCache.hasOwnProperty(groupName)) {
                groupCache[groupName].isCollapsed = true;
                me.expandcollapseGroup(groupCache[groupName], true);
            }
        }
        Ext.suspendLayouts();
        me.dataSource.onRefresh();

        if (lockingPartner && !lockingPartner.isAllCollapsed()) {
            lockingPartner.collapseAll();
        }
        Ext.resumeLayouts(true);

        for (groupName in groupCache) {
            if (groupCache.hasOwnProperty(groupName)) {
                me.afterCollapseExpand(true, groupName);
                if (lockingPartner) {
                    lockingPartner.afterCollapseExpand(true, groupName);
                }
            }
        }

    },

    doCollapseExpand: function(collapsed, groupName, focus) {
        var me = this,
            lockingPartner = me.lockingPartner,
            group = me.groupCache[groupName];

        if (group.isCollapsed !== collapsed) {

            Ext.suspendLayouts();
            if (collapsed) {
                me.dataSource.collapseGroup(group);
            } else {
                me.dataSource.expandGroup(group);
            }
            Ext.resumeLayouts(true);

            me.afterCollapseExpand(collapsed, groupName, focus);

            if (lockingPartner) {
                lockingPartner.afterCollapseExpand(collapsed, groupName, false);
            }
        }
    },

    afterCollapseExpand: function(collapsed, groupName, focus) {
        var me = this,
            view = me.view,
            header;

        header = me.getHeaderNode(groupName);

        view.fireEvent(collapsed ? 'groupcollapse' : 'groupexpand', view, header, groupName);
        if (focus) {
            if (header) {
                view.scrollElIntoView(Ext.fly(header).up(view.getItemSelector()), false, true);
            }            
            else if (view.bufferedRenderer) {
                view.bufferedRenderer.scrollTo(me.dataSource.indexOf(me.getGroup(groupName).getAt(0)));
            }
        }
        me.onViewScroll();
    },

    onGroupChange: function(store, grouper) {
        var me = this,
            ownerCt = me.grid.ownerCt,
            view = me.view;

        if (!grouper) {
            if (ownerCt && ownerCt.lockable) {
                ownerCt.view.refresh();
            } else {
                view.refresh();
            }
        } else {
            me.lastGrouper = grouper;
        }
    },

    getMenuItem: function(dataIndex){
        var view = this.view,
            header = view.headerCt.down('gridcolumn[dataIndex=' + dataIndex + ']'),
            menu = view.headerCt.getMenu();

        return header ? menu.down('menuitem[headerId='+ header.id +']') : null;
    },

    onGroupKey: function(keyCode, event) {
        var me = this,
            groupName = me.getGroupName(event.target);

        if (groupName) {
            me.onGroupClick(me.view, event.target, groupName, event);
        }
    },

    onGroupClick: function(view, rowElement, groupName, e) {
        var me = this,
            groupCache = me.groupCache,
            groupIsCollapsed = !me.isExpanded(groupName),
            g;

        if (me.collapsible) {
            if (e.ctrlKey) {
                Ext.suspendLayouts();
                for (g in groupCache) {
                    if (g === groupName) {
                        if (groupIsCollapsed) {
                            me.expand(groupName);
                        }
                    } else if (!groupCache[g].isCollapsed) {
                        me.doCollapseExpand(true, g, false);
                    }
                }
                Ext.resumeLayouts(true);
                return;
            }

            if (groupIsCollapsed) {
               me.expand(groupName);
            } else {
                me.collapse(groupName);
            }
        }
    },

    setupRowData: function(record, idx, rowValues) {
        var me = this,
            recordIndex = rowValues.recordIndex,
            data = me.refreshData,
            groupInfo = [],
            header = data.header,
            groupField = data.groupField,
            store = me.view.getStore(),
            dataSource = me.view.dataSource,
            grouper, groupName, prev, next, items,
            groupers, groupersCount, i, j, lastRowIndex, isLastRow, isFirstRow, 
            summaryRecords = new Ext.util.MixedCollection(),
            groupId, isCollapsedGroup, isLastCollapsed,
            rows, itemClasses = [], parent, group, mainGroupId, lastitem, noMoreHeaders, noMoreFooters,
            itemsCount;

        rowValues.isCollapsedGroup = false;
        rowValues.summaryRecords = rowValues.groupHeaderCls = null;

        if (data.doGrouping) {
            groupers = me.getGroupers();
            groupersCount = groupers.length;

            // 1) группа свернута - у нее есть placeholder
            if (record.isCollapsedPlaceholder) {
                group = record.group;
                if (group.parentGroup) {
                    items = group.parentGroup.items;
                    rowValues.isFirstRow = group === items[0];
                    rowValues.isLastRow  = group === items[items.length - 1];
                } else {
                    rowValues.isFirstRow = rowValues.isLastRow = true;
                }
                groupName = group.getGroupKey();
                groupId = me.getGroupKey(group);
                mainGroupId = groupId;
                rowValues.groupHeaderCls = me.hdCollapsedCls;
                rowValues.isCollapsedGroup = rowValues.needsWrap = true;
                rowValues.groupName = groupName;
                rowValues.groupInfo = groupInfo;
                rowValues.summaryRecords = [];
                if (me.showSummaryRow && !me.showSummaryRowOnDetails) {
                    rowValues.summaryRecords.unshift(data.summaryData[groupId]);
                }
                
                // посчитаем группы-родители
                i = 0;
                parent = group.parentGroup;
                while (parent) {
                    parent = parent.parentGroup;
                    i++;
                }
                // пробежимся по родителям
                noMoreHeaders = noMoreFooters = false;
                isLastRow = false;
                isFirstRow = false;
                while (group) {
                    groupName = group.getGroupKey();
                    groupId = me.getGroupKey(group);
                    groupField = group.grouper.getProperty();
                    header = me.getGroupedHeader(groupField);
                    if (!me.isExpanded(groupId)) {
                        itemClasses.push(me.hdCollapsedCls);
                        isCollapsedGroup = true;
                    } else {
                        isCollapsedGroup = false;
                    }
                    items = group.items;
                    // если строка первая в гриде или текущая, то шапку надо
                    if ((isFirstRow && !noMoreHeaders) || mainGroupId == groupId) {
                        itemsCount = me.getSummary(store, 'count', '', group);
                        groupInfo.unshift({
                            groupHeaderCls: isCollapsedGroup ? me.hdCollapsedCls : null,
                            isCollapsedGroup: true,
                            collapsibleCls: me.collapsible ? me.collapsibleCls : me.hdNotCollapsibleCls,
                            groupField: groupField,
                            groupId: groupId,   
                            name: groupName,
                            groupName: groupName,
                            groupValue: items[0].get(groupField),
                            columnName: header ? header.text : groupField,
                            itemsCount: itemsCount,
                            //children: items,
                            depth: me.depthToIndent * i,
                            level: i
                        });
                    }
                    // если была последняя запись в группе, то надо собрать итоги по родителям
                    if (isLastRow && !noMoreFooters && me.showSummaryRow) {
                        rowValues.summaryRecords.push(data.summaryData[groupId]);
                    }
                    
                    if (group.parentGroup) {
                        items = group.parentGroup.items;
                        isFirstRow = group === items[0];
                        isLastRow = group === items[items.length - 1];
                    } else {
                        isFirstRow = rowValues.isFirstRow;
                        isLastRow = rowValues.isLastRow;
                    }
                    
                    i--;
                    group = group.parentGroup;
                    // если не первая запись, значит больше не нужно заголовков групп
                    if (!isFirstRow) {
                        noMoreHeaders = true;
                    }
                    if (!isLastRow) {
                        noMoreFooters = true;
                    }
                    // Если не первая и не последняя, то не будем больше подниматься
                    if (!isFirstRow && !isLastRow) {
                        group = null;
                    }
                }
                return;
            }
        
            // если запись первая в группе, то для нее надо выводить шапку группы

            rowValues.groupInfo = groupInfo;
            group = me.getRecordGroup(record);
            groupName = group.getGroupKey();
            groupId = me.getGroupKey(group);
            mainGroupId = groupId;
            // посчитаем группы-родители
            i = 0;
            parent = group.parentGroup;
            while (parent) {
                parent = parent.parentGroup;
                i++;
            }
            // пробежимся по родителям
            lastitem = record;
            noMoreHeaders = noMoreFooters = false;
            while (group) {
                groupName = group.getGroupKey();
                groupId = me.getGroupKey(group);
                groupField = group.grouper.getProperty();
                header = me.getGroupedHeader(groupField);
                if (!me.isExpanded(groupId)) {
                    itemClasses.push(me.hdCollapsedCls);
                    isCollapsedGroup = true;
                } else {
                    isCollapsedGroup = false;
                }
                items = group.items;
                isFirstRow = lastitem === items[0];
                isLastRow = lastitem === items[items.length - 1];
                // если строка первая в гриде или текущая, то шапку надо
                if (isFirstRow && !noMoreHeaders) {
                    rowValues.isFirstRow = true;
                    itemsCount = me.getSummary(store, 'count', '', group);
                    groupInfo.unshift({
                        groupHeaderCls: isCollapsedGroup ? me.hdCollapsedCls : null,
                        isCollapsedGroup: true,
                        collapsibleCls: me.collapsible ? me.collapsibleCls : me.hdNotCollapsibleCls,
                        groupField: groupField,
                        groupId: groupId,   
                        name: groupName,
                        groupName: groupName,
                        groupValue: items[0].get(groupField),
                        columnName: header ? header.text : groupField,
                        itemsCount: itemsCount,
                        //children: items,
                        depth: me.depthToIndent * i,
                        level: i
                    });
                }
                // если была последняя запись в группе, то надо собрать итоги по родителям
                if (isLastRow && !noMoreFooters && me.showSummaryRow) {
                    summaryRecords.add(data.summaryData[groupId]);
                }
                i--;
                lastitem = group;
                group = group.parentGroup;
                // если не первая запись, значит больше не нужно заголовков групп
                if (!isFirstRow) {
                    noMoreHeaders = true;
                }
                if (!isLastRow) {
                    noMoreFooters = true;
                }
                // Если не первая и не последняя, то не будем больше подниматься
                if (!isFirstRow && !isLastRow) {
                    group = null;
                }
            }

            // если запись последняя в группе, то для нее надо выводить подвал группы
            if (me.showSummaryRow) {
                rowValues.summaryRecords = summaryRecords.getRange();
                itemClasses.push(Ext.baseCSSPrefix + 'grid-group-last');
            }
            rowValues.itemClasses = itemClasses;
            rowValues.collapsibleCls = me.collapsible ? me.collapsibleCls : me.hdNotCollapsibleCls;
            rowValues.groupInfo = groupInfo;
            rowValues.isCollapsedGroup = isCollapsedGroup;
            rowValues.needsWrap = (rowValues.isFirstRow || rowValues.summaryRecords);
        }
    },

    setup: function(rows, rowValues) {
        var me = this,
            data = me.refreshData,
            view = rowValues.view,
            isGrouping = view.isGrouping = !me.disabled && me.getGroupers() && me.getGroupers().length > 0;

        me.skippedRows = 0;
        if (view.bufferedRenderer) {
            view.bufferedRenderer.variableRowHeight = view.bufferedRenderer.variableRowHeight || isGrouping;
            view.bufferedRenderer.scrollTo = me.scrollTo;
        }
        data.doGrouping = isGrouping;
        rowValues.groupHeaderTpl = Ext.XTemplate.getTpl(me, 'groupHeaderTpl');

        if (isGrouping && me.showSummaryRow) {
            data.summaryData = me.generateSummaryData();
        }
        me.lastDepth = 0;
    },

    cleanup: function(rows, rowValues) {
        var data = this.refreshData;

        rowValues.groupInfo = rowValues.groupHeaderTpl = rowValues.isFirstRow = null;
        data.groupField = data.header = null;
    },

    getGroupInfo: function (group) {
        var groupInfo = this.getGroupings(),
            key = this.getGroupKey(group),
            item = groupInfo[key];

        if (!item) {
            item = groupInfo[key] = {
                lastGroupGeneration: null,
                lastFilterGeneration: null,
                aggregateRecord: new Ext.data.Model()
            };
        }

        return item;
    },

    getGroupings: function () {
        return this.groupInfo;
    },

    getAggregateRecord: function(groupInfo, forceNew) {
        var rec;

        if (forceNew === true || !groupInfo.aggregateRecord) {
            rec = new Ext.data.Model();
            groupInfo.aggregateRecord = rec;
            rec.isNonData = rec.isSummary = true;
        }

        return groupInfo.aggregateRecord;
    },

    generateSummaryData: function(){
        var me = this,
            store = me.view.store,
            filters = store.getFilters(),
            groups = me.dataSource.getGroups(),
            reader = store.getProxy().getReader(),
            groupField = me.getGroupField(),
            lockingPartner = me.lockingPartner,
            updateNext = me.updateNext,
            data = {},
            i, len, group, groupInfo, record, hasRemote, remoteData;

        if (me.remoteRoot && reader.rawData) {
            hasRemote = true;
            remoteData = me.mixins.summary.generateSummaryData.call(me, groupField);
        }

        for (i = 0, len = groups.length; i < len; ++i) {
            group = groups[i];
            groupInfo = me.getGroupInfo(group);

            if (updateNext || hasRemote || store.updating || ((groupInfo.lastGroupGeneration !== group.generation) || groupInfo.lastFilterGeneration !== filters.generation)) {
                record = me.populateRecord(group, groupInfo, remoteData);
                record.level = 0;

                if (!lockingPartner || (me.view.ownerCt === me.view.ownerCt.ownerLockable.normalGrid)) {
                    groupInfo.lastGroupGeneration = group.generation;
                    groupInfo.lastFilterGeneration = filters.generation;
                }

            } else {
                record = me.getAggregateRecord(groupInfo);
                record.level = 0;
            }

            data[me.getGroupKey(group, this.getGroupers())] = record;
            me.generateGroupSummary(data, group, 1);
        }

        me.updateNext = false;

        return data;
    },

    generateGroupSummary: function(data, groups, level){
        var me = this, 
            i, len, record, groupInfo, filters, group;
        if (!groups.isCollapsed && groups.withSubGroups) {
            for (i = 0, len = groups.length; i < len; ++i) {
                group = groups.getAt(i);
                groupInfo = me.getGroupInfo(group);
                record = me.populateRecord(group, groupInfo);
                record.level = level;

                data[me.getGroupKey(group)] = record;
                me.generateGroupSummary(data, group, level + 1);
            }
        }
    },

    getGroupName: function(element) {
        var me = this,
            view = me.view,
            eventSelector = me.eventSelector,
            parts,
            targetEl,
            row;

        targetEl = Ext.fly(element).findParent(eventSelector);

        if (!targetEl) {
            row = Ext.fly(element).findParent(view.itemSelector);
            if (row) {
                targetEl = row.down(eventSelector, true);
            }
        }

        if (targetEl) {
            return Ext.htmlDecode(targetEl.getAttribute('data-groupname'));
        }
    },

    getRecordGroup: function(record) {
        var groupName = this.getGroupKey(record, this.getGroupers());
        return this.groupCache[groupName];
    },

    getGroupField: function(){
        return this.getGroupers() && this.getGroupers().length > 0 ? this.getGroupers()[0].getProperty() : null;
    },

    getGroupedHeader: function(groupField) {
        var me = this,
            headerCt = me.view.headerCt,
            partner = me.lockingPartner,
            selector, header;

        groupField = groupField || this.getGroupField();

        if (groupField) {
            selector = '[dataIndex=' + groupField + ']';
            header = headerCt.down(selector);
            if (!header && partner) {
                header = partner.view.headerCt.down(selector);
            }
        }
        return header || null;
    },

    getFireEventArgs: function(type, view, targetEl, e) {
        return [type, view, targetEl, this.getGroupName(targetEl), e];
    },

    destroy: function() {
        var me = this,
            dataSource = me.dataSource;

        Ext.destroy(me.storeListeners);
        me.view = me.prunedHeader = me.grid = me.groupCache = me.dataSource = null;
        me.callParent();
        if (dataSource) {
            dataSource.bindStore(null);
            Ext.destroy(dataSource);
        }
    },

    beforeReconfigure: function(grid, store, columns, oldStore, oldColumns) {
        var me = this,
            view = me.view,
            dataSource = me.dataSource,
            bufferedStore;

        if (store && store !== oldStore) {
            bufferedStore = store.isBufferedStore;

            if (!dataSource) {
                Ext.destroy(me.storeListeners);
                me.storeListeners = store.on({
                    groupchange: me.onGroupChange,
                    scope: me,
                    destroyable: true
                });
            }

            if (bufferedStore !== oldStore.isBufferedStore) {
                Ext.Error.raise('Cannot reconfigure grouping switching between buffered and non-buffered stores');
            }

            view.isGrouping = !!me.getGroupers();
            dataSource.bindStore(store);
        }
    },

    populateRecord: function (group, groupInfo, remoteData) {
        var me = this,
            view = me.grid.ownerLockable ? me.grid.ownerLockable.view : me.view,
            store = me.view.getStore(),
            record = me.getAggregateRecord(groupInfo),
            columns = view.headerCt.getGridColumns(),
            len = columns.length,
            groupName = me.getGroupKey(group),
            groupData, field, i, column, fieldName, summaryValue;

        record.beginEdit();

        if (remoteData) {
            groupData = remoteData[groupName];
            for (field in groupData) {
                if (groupData.hasOwnProperty(field)) {
                    if (field !== record.idProperty) {
                        record.set(field, groupData[field]);
                    }
                }
            }
        }

        for (i = 0; i < len; ++i) {
            column = columns[i];
            fieldName = column.dataIndex || column.getItemId();

            if (!remoteData) {
                summaryValue = me.getSummary(store, column.summaryType, fieldName, group);
                record.set(fieldName, summaryValue);
            } else {
                summaryValue = record.get(column.dataIndex);
            }

            me.setSummaryData(record, column.getItemId(), summaryValue, groupName);
        }

        record.ownerGroup = groupName;

        record.endEdit(true);
        record.commit();

        return record;
    },

    getGroupers: function () {
        return this.view.store.groupers;
    },

    getGroupKey: function (record, groupers) {
        if (!groupers) {
            groupers = this.getGroupers();
        }
        var groupersCount = groupers.length,
            i, keys = [], parent;

        if (record instanceof Ext.util.Group) {
            parent = record.parentGroup;
            keys.push(record.getGroupKey());
            while (parent) {
                keys.push(parent.getGroupKey());
                parent = parent.parentGroup;
            }
        } else {
            for (i = 0; i < groupersCount; i++) {
                keys.unshift(groupers[i].getGroupString(record));
            }
        }
        return this.formatKey.apply(this, keys);
    },

    formatKey: function () {
        var i, s = '', a = '';
        for (i = 0; i < arguments.length; i++) {
            if (i > 0 && s) s += '#_#';
            a = Ext.isEmpty(arguments[i]) ? '#_#' : arguments[i];
            s += a.toString().replace(/[^A-Za-z0-9_А-Яа-я#]/gi, '_');
        }
        return s;
    },

    getAllSubItems: function(group) {
        var items, i;
        if (group.withSubGroups) {
            items = new Ext.util.Collection({rootProperty: 'data'});
            for (i = 0; i < group.length; i++) {
                items.add(this.getAllSubItems(group.getAt(i)).items);
            }
            return items;
        } else 
            return group;
    },

    getSummary: function(store, type, field, group) {
        var isGrouped = !!group,
            item = isGrouped ? group : store;
        if (group && group.withSubGroups){
            item = this.getAllSubItems(group);
        }
        if (type) {
            if (Ext.isFunction(type)) {
                if (isGrouped) {
                    return item.aggregate(field, type);
                } else {
                    return item.aggregate(type, null, false, [
                        field
                    ]);
                }
            }
            switch (type) {
                case 'count':
                    return item.count(field);
                case 'min':
                    return item.min(field);
                case 'max':
                    return item.max(field);
                case 'sum':
                    return item.sum(field);
                case 'average':
                    return item.average(field);
                default:
                    return '';
            }
        }
    },

    outputSummaryRecord: function(summaryRecord, contextValues, out) {
        var view = contextValues.view,
            savedRowValues = view.rowValues,
            columns = contextValues.columns || view.headerCt.getVisibleGridColumns(),
            colCount = columns.length, i, column,
            depth = summaryRecord.level ? this.depthToIndent * summaryRecord.level : 0,
            values = {
                view: view,
                record: summaryRecord,
                rowStyle: '',
                rowClasses: [ this.summaryRowCls ],
                itemClasses: [],
                recordIndex: -1,
                rowId: view.getRowId(summaryRecord),
                columns: columns
                //rowAttr: depth > 0 ? 'style = "display: table-row; margin-left: ' + depth + 'px;"' : null
            };

        for (i = 0; i < colCount; i++) {
            column = columns[i];
            column.savedRenderer = column.renderer;
            column.renderer = this.createRenderer(column, summaryRecord);
        }

        view.rowValues = values;
        view.self.prototype.rowTpl.applyOut(values, out, parent);
        view.rowValues = savedRowValues;

        for (i = 0; i < colCount; i++) {
            column = columns[i];
            column.renderer = column.savedRenderer;
            column.savedRenderer = null;
        }
    },

    createRenderer: function (column, record) {
        var me = this,
            ownerGroup = record.ownerGroup,
            summaryData = ownerGroup ? me.summaryData[ownerGroup] : me.summaryData,
            dataIndex = column.dataIndex || column.getItemId(),
            depth = record.level ? me.depthToIndent * record.level : 0;

        return function (value, metaData, record) {
            if (column.isFirstVisible) {
                metaData.style = depth > 0 ? 'margin-left: ' + depth + 'px;' : null;
            }

            return column.summaryRenderer ?
                column.summaryRenderer(record.data[dataIndex], summaryData, dataIndex, metaData) :
                record.data[dataIndex];
        };
    },

    getMetaGroup: function (group) {
        return this.getGroup(group);
    },

    scrollTo: function(recordIdx, options) {
        var args = arguments,
            me = this,
            view = me.view,
            viewDom = view.el.dom,
            store = me.store,
            total = store.getCount(),
            startIdx, endIdx,
            targetRow,
            tableTop,
            groupingFeature,
            group,
            record,
            direction,
            scrollDecrement = 0,
            doSelect,
            doFocus,
            animate,
            highlight,
            callback,
            scope;

        if (options && typeof options === 'object') {
            doSelect = options.select;
            doFocus = options.focus;
            highlight = options.highlight;
            animate = options.animate;
            callback = options.callback;
            scope = options.scope;
        }
        else {
            doSelect = args[1];
            callback = args[2];
            scope = args[3];
        }

        if ((groupingFeature = view.dataSource.groupingFeature) && (groupingFeature.collapsible)) {

            if (recordIdx.isEntity) {
                record = recordIdx;
            } else {
                record = view.dataSource.getAt(Math.min(Math.max(recordIdx, 0), view.dataSource.getCount() - 1));
            }

            recordIdx = groupingFeature.indexOf(record);

        } else {

            if (recordIdx.isEntity) {
                record = recordIdx;
                recordIdx = store.indexOf(record);

                if (recordIdx === -1) {
                    Ext.Error.raise('Unknown record passed to BufferedRenderer#scrollTo');
                    return;
                }
            } else {
                recordIdx = Math.min(Math.max(recordIdx, 0), total - 1);
                record = store.getAt(recordIdx);
            }
        }

        if (record && (targetRow = view.getNode(record))) {
            view.getScrollable().scrollIntoView(targetRow, null, animate, highlight);

            me.onViewScroll();

            if (doSelect) {
                view.selModel.select(record);
            }
            if (doFocus) {
                view.getNavigationModel().setPosition(record, 0);
            }
            if (callback) {
                callback.call(scope||me, recordIdx, record, targetRow);
            }
            return;
        }

        if (recordIdx < view.all.startIndex) {
            direction = -1;
            startIdx = Math.max(Math.min(recordIdx - (Math.floor((me.leadingBufferZone + me.trailingBufferZone) / 2)), total - me.viewSize + 1), 0);
            endIdx = Math.min(startIdx + me.viewSize - 1, total - 1);
        }
        else {
            direction = 1;
            endIdx = Math.min(recordIdx + (Math.floor((me.leadingBufferZone + me.trailingBufferZone) / 2)), total - 1);
            startIdx = Math.max(endIdx - (me.viewSize - 1), 0);
        }
        tableTop = Math.max(startIdx * me.rowHeight, 0);

        store.getRange(startIdx, endIdx, {
            callback: function(range, start, end) {
                var scroller = view.getScrollable();

                me.renderRange(start, end, true, true);

                record = store.data.getRange(recordIdx, recordIdx + 1)[0];
                targetRow = view.getNode(record);

                if (!targetRow) {
                    return;
                }

                view.body.translate(null, me.bodyTop = tableTop);
                view.suspendEvent('scroll');

                if (direction === 1) {
                    scrollDecrement = viewDom.clientHeight - targetRow.offsetHeight;
                }
                me.position = me.scrollTop = Math.min(Math.max(0, tableTop - view.body.getOffsetsTo(targetRow)[1]) - scrollDecrement, scroller.getSize().y - viewDom.clientHeight);

                scroller.scrollIntoView(targetRow, null, animate, highlight);
                view.resumeEvent('scroll');

                if (doSelect) {
                    view.selModel.select(record);
                }
                if (doFocus) {
                    view.getNavigationModel().setPosition(record, 0);
                }
                if (callback) {
                    callback.call(scope||me, recordIdx, record, targetRow);
                }
            }
        });
    },

    onViewScroll: function() {
        var me = this,
            groupTitleSelector = '.'+Ext.baseCSSPrefix + 'grid-group-hd',
            titlesEl, dx, x, y, el, i;
        titlesEl = Ext.query(groupTitleSelector, true, me.view.body.el);
        dx = this.view.getScrollX();
        for (i = 0; i < titlesEl.length; i++) {
            el = Ext.get(titlesEl[i]);
            x = el.translateXY(0, 0).x;
            el.setX(dx-x);
        }
    },

    hasActiveFeature: function() {
        return (this.isGrouping && this.dataSource.isGrouped()) || this.isRowWrapped;
    },

    onUpdate: function(store, record, operation, modifiedFieldNames, details) {
        var me = this,
            isFiltered = details && details.filtered;
        if (record.placeholder) {
            record = record.placeholder;
        }
        if (!isFiltered && me.getNode(record)) {
            if (me.throttledUpdate) {
                me.statics().queueRecordChange(me, store, record, operation, modifiedFieldNames);
            } else {
                me.handleUpdate.apply(me, arguments);
            }
        }
    }
});