Ext.define('BarsUp.grid.multigrouping.MultigroupToExcel', {
    extend: 'Ext.util.Observable',

    requires: [
        'BarsUp.utils.ExcelFile',
        'BarsUp.utils.Tasks'
    ],

    sheetName: 'test',
    fileName: 'test',

    spaceWidth: 15,

    groupStyle: {
        backColor: 'E1E8F1',
        textColor: '000000',
        borderColor: 'b5b8c8',
        fontSize: 8
    },

    summaryStyle: {
        backColor: 'F9F9F9',
        textColor: '000000',
        borderColor: 'b5b8c8',
        fontSize: 8
    },

    columnStyle: {
        backColor: 'F9F9F9',
        textColor: '000000',
        borderColor: 'c5c5c5',
        fontSize: 8
    },

    cellStyle: {
        backColor: 'FFFFFF',
        textColor: '000000',
        borderColor: 'b5b8c8',
        fontSize: 8
    },

    exportColumn: function (col) {
        var style = this.columnStyle,
            bc = style.borderColor;

        return {
            value: col.header,
            colSpan: col.colSpan,
            rowSpan: col.rowSpan,
            vAlign: 'center',
            hAlign: 'center',
            borders: {
                bottom: bc,
                right: bc,
                left: bc,
                top: bc
            },
            wrapText: true,
            bold: true,
            width: col.width,
            fontSize: style.fontSize,
            bgColor: style.backColor,
            color: style.textColor,
            height: col.rowSpan < 2 ? 26 : null
        };
    },

    exportRow: function(row){
        var r = [],
            extra = {},
            style = this.cellStyle;

        if(row.isSummary){
            var i = row.isRoot ? this._space
                : (this._space - row.level + (row.parent.expanded ? 0 : 1));

            if(i){
                r.push(this.getGroupSpace({
                    width: this.spaceWidth,
                    value: '',
                    colSpan: i
                }, { left: false }));
            }

            style = this.summaryStyle;
            extra.bold = true;
            extra.height = 26;
            extra.vAlign = 'center';
        }

        Ext.each(this._cols, function(col){
            var conf = {
                //vAlign: x.getStyle('verticalAlign'),
                value: '',
                fontSize: style.fontSize,
                bgColor: style.backColor,
                color: style.textColor,
                borders: {
                    bottom: style.borderColor,
                    right: style.borderColor,
                    left: style.borderColor,
                    top: style.borderColor
                },
                wrapText: false,
                width: col.width
            };

            if(col.column){
                conf.value = row.get(col.dataIndex);
                conf.hAlign = col.column.textAlign;
                conf.formatCode = col.column.format;
            }

            Ext.apply(conf, extra);

            r.push(conf);
        });

        var d = this._data,
            l = d.length;

        if(row.parent && row.parent.rows.indexOf(row) == 0){
            d[l - 1] = d[l - 1].concat(r);
        } else {
            d.push(r);
        }
    },

    getGroupSpace: function(data, b){
        var style = this.groupStyle,
            bc = style.borderColor;

        b = b || {};

        return Ext.apply({
            fontSize: style.fontSize,
            bgColor: style.backColor,
            color: style.textColor,
            borders: {
                bottom: b.bottom === false ? style.backColor : bc,
                right: b.right === false ? style.backColor : bc,
                left: b.left === false ? style.backColor : bc,
                top: b.top === false ? style.backColor : bc
            }
        }, data);
    },

    exportGroupRow: function(row){
        var style = this.groupStyle,
            bc = style.borderColor;

        var r = [
            this.getGroupSpace({
                vAlign: 'center',
                bold: true,
                value: row.getGroupText(),
                height: 26,
                colSpan: this._cols.length + (this._space - row.level)
            }, { bottom: false })
        ];

        var d = this._data,
            l = d.length;

        if(row.parent && row.parent.rows.indexOf(row) == 0){
            d[l - 1] = d[l - 1].concat(r);
        } else {
            d.push(r);
        }

        if(row.expanded){
            d.push([
                this.getGroupSpace({
                    value: '',
                    width: this.spaceWidth,
                    rowSpan: row.getRowSpan() - 1,
                    border: {
                        top: style.backColor
                    }
                }, { top: false, right: false })
            ]);
        }
    },

    start: function (grid) {
        var tasks = [],
            me = this,
            max = 999999999999,
            wnd = this.getExportWindow(),
            delta;

        me.grid = grid;
        me.groupingfeature = grid.getView().getFeature('group');

        wnd.show();

        this._data = [];
        this._cols = [];
        this._space = 0;

        me.getData(0, 0, max, max, function(res){
            var exportColumn = function(col, lvl){
                var conf = me.exportColumn(col);

                me._data[lvl] = me._data[lvl] || [];
                me._data[lvl].push(conf);

                if(!col.column) {
                    conf.width = me.spaceWidth;
                    me._space++;
                    return;
                }

                if(col.column.isLeaf()){
                    me._cols.push(col);
                } else {
                    Ext.each(col.columns, function(x){
                        exportColumn(x, lvl + 1);
                    });
                }
            };

            tasks.push(function () {
                Ext.each(res.columns.locked, function(x){ exportColumn(x, 0); });
                Ext.each(res.columns.unlocked, function(x){ exportColumn(x, 0); });
            });

            var createRow = function(x){
                if(x.isGroupRow){
                    tasks.push(function () {
                        me.exportGroupRow(x);
                    });

                    if(x.expanded){
                        Ext.each(x.rows, function(r){
                            createRow(r);
                        });
                    } else {
                        //createRow(x.sumRow);
                    }
                } else {
                    tasks.push(function () {
                        me.exportRow(x);
                    });
                }
            };

            Ext.each(res.rows, function(c){ createRow(c.row) });
        });

        this.fireEvent('start', this, grid);

        delta = tasks.length / 100;

        BarsUp.utils.Tasks.start(tasks,
            null,
            function () {
                wnd.progressBar.updateProgress(1);
                wnd.progressBar.updateText('Подготовка файла...');

                setTimeout(function(){
                    me.end();
                    wnd.destroy();
                }, 50);
            },
            null,
            function (task, ln) {
                var p = 100 - ln / delta;

                me.fireEvent('process', p, me);

                wnd.progressBar.updateText(p.toFixed(2) + ' %');
                wnd.progressBar.updateProgress(p / 100);
            }
        );
    },

    end: function () {
        var file = Ext.create('BarsUp.ExcelFile', {
            worksheets: [
                {
                    name: this.sheetName,
                    //table: this.inTable,
                    //tableStart: this.rowspan,
                    data: this._data
                }
            ]
        });

        this.fireEvent('process', 100, this);
        this.fireEvent('end', this);

        this.destroy();

        file.download({ fileName: this.fileName });
    },

    getExportWindow: function(){
        var progress = Ext.create('Ext.ProgressBar', {
                text: 'Экспорт в Excel'
            });

        return new Ext.Window({
            closable: false,
            collapsible: false,
            draggable: false,
            resizable: false,
            width: 300,
            height: 60,
            modal: true,
            plain: true,
            title: 'Экспорт в Excel',
            progressBar: progress,
            items: [progress]
        });
    },

    getData: function(startX, startY, endX, endY, callback) {
        var me = this,
            store = me.grid.getView().dataSource,
            len = store.getCount(),
            i, j, k, record, row, col, useRenderer, v, rowValues,
            //columns = me.grid.getColumns(), 
            columns = me.grid.headerCt.items,
            headers = [],
            group, sumrec, groupHeaderTpl,
            groupname,
            result = {
                columns: {
                    locked: [],
                    unlocked: []
                },
                rows: []
            },
            lines = me.getColumns(columns, headers, 1);
        if (me.groupingfeature && store.isGrouped()) {
            groupHeaderTpl = Ext.XTemplate.getTpl(me.groupingfeature, 'groupHeaderTpl');
        }
        var makeColumn = function(col, lines) {
            var resCol = {
                    header: col.text,
                    colSpan: 1,
                    rowSpan: lines,
                    width: col.getWidth(),
                    dataIndex: col.dataIndex,
                    column: {    
                        textAlign: col.textAlign,
                        format: col.format,
                        leaf: true,
                        isLeaf: function () {
                          return this.leaf;
                        }
                    }
                }, i, len, whole, subcol;
            if (col.isGroupHeader) {
                resCol.columns = [];
                whole = 0;
                len = col.items.items.length;
                for (i = 0; i < len; i++) {
                    if (col.items.items[i].isVisible()) {
                        subcol = makeColumn(col.items.items[i], lines-1);
                        resCol.columns.push(subcol);
                        whole = whole + subcol.colSpan;
                    }
                }
                resCol.column.leaf = false;
                resCol.colSpan = whole;
                resCol.rowSpan = resCol.rowSpan - 1;
            }
            return resCol;
        };
        Ext.each(columns.items, function (x) {
            if (x.isVisible()) {
                result.columns.unlocked.push(makeColumn(x, lines));
            }
        });

        for(i = 0; i < len; i++){
            record = store.getAt(i);
            rowValues = {};
            if (me.groupingfeature && store.isGrouped()) {
                me.groupingfeature.setupRowData(record, 0, rowValues);
                // rowValues.groupInfo - массив заголовков 
                for (k = 0; k < rowValues.groupInfo.length; k++) {
                    group = rowValues.groupInfo[k];
                    groupname = groupHeaderTpl.apply(group);
                    row = {
                        row: {
                            isGroupRow: true,
                            expanded: false, //!group.isCollapsedGroup,
                            isSummary: false,
                            isRoot: true,
                            parent: undefined,
                            _groupText: new Array(group.level * 7 + 1).join("&#160;") + groupname,
                            get: function (fieldName) {
                                return this[fieldName];
                            },
                            getGroupText: function() {
                                return this._groupText;
                            },
                            sumRow: {
                                get: function (fieldName) {
                                    return this[fieldName];
                                }
                            }
                        }
                    };
                    result.rows.push(row);
                }
            }

            if (!record.isCollapsedPlaceholder) {
                row = {
                    row: {
                        isSummary: false,
                        isRoot: true,
                        parent: undefined,
                        get: function (fieldName) {
                            return this[fieldName];
                        }
                    }
                };
                // если просто запись
                for(j = 0; j < headers.length; j++){
                    col = headers[j];
                    // не использовать рендерер для числовых полей
                    useRenderer = (col.xtype != 'numbercolumn') && (col.renderer || (!Ext.isEmpty(col.initialConfig.formatter) && Ext.isEmpty(col.formatter)));
                    v = record.get(col.dataIndex) || '';
                    row.row[col.dataIndex] = useRenderer ? col.renderer(v, {record: record, column: col}, record) : v;
                }
                result.rows.push(row);
            }
            // rowValues.summaryRecords - массив итогов
            if (rowValues.summaryRecords) {
                for (k = 0; k < rowValues.summaryRecords.length; k++) {
                    sumrec = rowValues.summaryRecords[k];
                    row = {
                        row: {
                            isSummary: true,
                            isRoot: true,
                            parent: undefined,
                            get: function (fieldName) {
                                return this[fieldName];
                            },
                            sumRow: {
                                get: function (fieldName) {
                                    return this[fieldName];
                                }
                            }
                        }
                    };
                    for(j = 0; j < headers.length; j++){
                        col = headers[j];
                        // не использовать рендерер для числовых полей
                        useRenderer = (col.xtype != 'numbercolumn') && (col.renderer || (!Ext.isEmpty(col.initialConfig.formatter) && Ext.isEmpty(col.formatter)));
                        v = sumrec.get(col.dataIndex) || '';
                        row.row[col.dataIndex] = useRenderer ? col.renderer(v, {record: record, column: col}, record) : v;
                    }
                    result.rows.push(row);
                }
            }
        }

        callback(result);
    },
    getColumns: function(columns, result, level){
        var i, col, l, r = level;
        for(i = 0; i < columns.length; i++){
            col = columns.get(i);
            if (col.isGroupHeader) {
                l = this.getColumns(col.items, result, level+1);
                if (l > r) {
                    r = l;
                }
            } else {
                result.push(col);
            }
        }
        return r;
    },
});
