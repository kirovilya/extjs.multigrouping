Ext.define('BarsUp.grid.multigrouping.MultiGroupingSummary', {

    extend: 'BarsUp.grid.multigrouping.MultiGrouping',

    alias: 'feature.multigroupingsummary',

    showSummaryRow: true,
    
    vetoEvent: function(record, row, rowIndex, e){
        var result = this.callParent(arguments);
        if (result !== false && e.getTarget(this.summaryRowSelector)) {
            result = false;
        }
        return result;
    }
});