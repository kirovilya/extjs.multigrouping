Ext.define('BarsUp.grid.multigrouping.MultiGroupingStore', {
    extend: 'Ext.util.Observable',

    isStore: true,

    defaultViewSize: 100,

    isFeatureStore: true,

    constructor: function(groupingFeature, store) {
        var me = this;

        me.callParent();
        me.groupingFeature = groupingFeature;
        me.bindStore(store);
    },

    bindStore: function(store) {
        var me = this;

        if (!store || me.store !== store) {
            Ext.destroy(me.storeListeners);
            me.store = null;
        }
        if (store) {
            me.storeListeners = store.on({
                groupchange: me.onGroupChange,
                remove: me.onRemove,
                add: me.onAdd,
                idchanged: me.onIdChanged,
                update: me.onUpdate,
                refresh: me.onRefresh,
                clear: me.onClear,
                scope: me,
                destroyable: true
            });
            store.sort = me.sort;
            me.store = store;
            me.processStore(store);
        }
    },

    sort: function(field, direction, mode) {
        var me = this,
            sorter;
        if (arguments.length === 0) {
            if (me.getRemoteSort()) {
                me.attemptLoad();
            } else {
                me.forceLocalSort();
            }
        } else {
            // если нет направления сортировки и по полю уже сортируется
            // и это не первое поле для сортировки, то надо переключить сортировку
            // (это какой-то косяк в extjs)
            if (!direction) {
                sorter = me.getSorters().get(field);
                if (sorter && sorter != me.getAt(0)) {
                    sorter.toggle();
                    direction = sorter.getDirection();
                }
            }
            me.getSorters().addSort(field, direction, mode);
        }
    },

    processStore: function(store) {
        this.groupCache = null;
        var me = this,
            groups = me.getGroups(),
            groupCount = groups ? groups.length : 0,
            i,
            group,
            groupPlaceholder,
            data = me.data,
            oldGroupCache = me.groupingFeature.groupCache,
            groupCache = me.groupingFeature.clearGroupCache(),
            collapseAll = me.groupingFeature.startCollapsed, 
            groupField = store.getGroupField(),
            key, modelData, Model;

        if (data) {
            data.clear();
        } else {
            data = me.data = new Ext.util.Collection({
                rootProperty: 'data',
                extraKeys: {
                    byInternalId: {
                        property: 'internalId',
                        rootProperty: ''
                    }
                }
            });
        }

        if (store.getCount()) {
            if (groupCount > 0) {
                for (i = 0; i < groupCount; i++) {
                    group = groups[i];
                    me.processGroup(store, group, oldGroupCache, groupCache);
                }
            } else {
                data.add(store.getRange());
            }
        }
    },

    processGroup: function(store, group, oldGroupCache, groupCache) {
        var me = this,
            data = me.data,
            collapseAll = me.groupingFeature.startCollapsed,
            key, modelData, Model, groupPlaceholder,
            i, gr;

        key = me.groupingFeature.getGroupKey(group);
        groupCache[key] = group;
        if (oldGroupCache[key]) {
            group.isCollapsed = oldGroupCache[key].isCollapsed;
        } else {
            group.isCollapsed = collapseAll;
        }

        if (group.isCollapsed) {
            Model = store.getModel();
            modelData = {};
            modelData[group.grouper.getProperty()] = key;
            group.placeholder = groupPlaceholder = new Model(modelData);
            groupPlaceholder.isNonData = groupPlaceholder.isCollapsedPlaceholder = true;
            groupPlaceholder.group = group;
            data.add(groupPlaceholder);
        }
        else {
            if (group.withSubGroups) {
                for (i = 0; i < group.items.length; i++) {
                    gr = group.getAt(i);
                    me.processGroup(store, gr, oldGroupCache, groupCache);
                }
            } else {
                data.insert(data.length, group.items);
            }
        }
    },

    isCollapsed: function(name) {
        return this.groupingFeature.groupCache[name].isCollapsed; 
    },

    isInCollapsedGroup: function(record) {
        var store = this.store,
            groupData;

        if (this.isGrouped() && (groupData = this.groupingFeature.groupCache[record.get(store.getGroupField())])) {
            return groupData.isCollapsed || false;
        }
        return false;
    },

    isLoading: function() {
        return false;
    },

    getData: function() {
        return this.data;
    },

    getCount: function() {
        return this.data.getCount();
    },

    getTotalCount: function() {
        return this.data.getCount();
    },

    rangeCached: function(start, end) {
        return end < this.getCount();
    },

    getRange: function(start, end, options) {
        var result = this.data.getRange(start, Ext.isNumber(end) ? end + 1 : end);

        if (options && options.callback) {
            options.callback.call(options.scope || this, result, start, end, options);
        }
        return result;
    },

    getAt: function(index) {
        return this.data.getAt(index);
    },

    getById: function(id) {
        return this.store.getById(id);
    },

    getByInternalId: function (internalId) {
        return this.store.getByInternalId(internalId) || this.data.byInternalId.get(internalId);
    },

    expandGroup: function(group) {
        var me = this,
            startIdx, items, i, gr, key;

        if (typeof group === 'string') {
            group = me.groupingFeature.groupCache[group];
        }
        
        if (group) {
            items = group.items;
        }

        if (items.length && (startIdx = me.data.indexOf(group.placeholder)) !== -1) {

            group.isCollapsed = false;
            me.isExpandingOrCollapsing = 1;
            
            me.data.removeAt(startIdx);

            if (group.withSubGroups) {
                for (i = 0; i < group.items.length; i++) {
                    gr = group.getAt(i);
                    gr.isCollapsed = true;
                    me.data.insert(startIdx + i, gr.placeholder);
                    key = this.groupingFeature.getGroupKey(gr);
                    me.groupingFeature.groupCache[key] = gr;
                }
            } else {
                me.data.insert(startIdx, group.items);
            }

            me.fireEvent('groupchange', me);
            me.fireEvent('replace', me, startIdx, [group.placeholder], group.items);
            me.fireEvent('groupexpand', me, group);

            me.isExpandingOrCollapsing = 0;
        }
    },

    collapseGroup: function(group) {
        var me = this,
            startIdx, endIdx,
            placeholder,
            len, items;

        if (typeof group === 'string') {
            group = me.groupingFeature.groupCache[group];
        }
        
        if (group) {
            items = group.items;
        }
        if (items) {
            startIdx = me.getGroupStart(group);
            endIdx = me.getGroupEnd(group);
            len = endIdx - startIdx + 1;
        }

        if (items && len && (startIdx !== -1)) {
            group.isCollapsed = true;
            me.isExpandingOrCollapsing = 2;

            me.data.removeAt(startIdx, len);

            me.data.insert(startIdx, placeholder = me.getGroupPlaceholder(group));

            me.fireEvent('groupchange', me);
            me.fireEvent('replace', me, startIdx, items, [placeholder]);
            me.fireEvent('groupcollapse', me, group);

            me.isExpandingOrCollapsing = 0;
        }
    },

    getGroupStart: function (group) {
        var me = this,
            items = group.items,
            startItem = items.length > 0 ? items[0] : null;
        if (startItem) {
            if (startItem.placeholder) {
                if (startItem.isCollapsed) {
                    return me.data.indexOf(startItem.placeholder);
                } else {
                    return me.getGroupStart(startItem);
                }
            } else {
                return me.data.indexOf(startItem);
            }
        } else 
            return -1;
    },

    getGroupEnd: function (group) {
        var me = this,
            items = group.items,
            endItem = items.length > 0 ? items[items.length-1] : null;
        if (endItem) {
            if (endItem.placeholder) {
                if (endItem.isCollapsed) {
                    return me.data.indexOf(endItem.placeholder);
                } else {
                    return me.getGroupEnd(endItem);
                }
            } else {
                return me.data.indexOf(endItem);
            }
        } else 
            return -1;
    },

    getGroupPlaceholder: function(group) {
        if (!group.placeholder) {
            var store = this.store,
                Model = store.getModel(),
                modelData = {},
                key = group.getGroupKey(),
                groupPlaceholder;

            modelData[store.getGroupField()] = key;
            groupPlaceholder = group.placeholder = new Model(modelData);
            groupPlaceholder.isNonData = groupPlaceholder.isCollapsedPlaceholder = true;
            groupPlaceholder.group = group;
        }
        return group.placeholder;
    },

    indexOf: function(record) {
        if (!record.isCollapsedPlaceholder) {
            return this.data.indexOf(record);
        }
        return -1;
    },

    indexOfId: function(id) {
        return this.data.indexOfKey(id);
    },

    indexOfTotal: function(record) {
        return this.store.indexOf(record);
    },

    onRefresh: function(store) {
        this.processStore(this.store);
        this.fireEvent('refresh', this);
    },

    onRemove: function(store, records, index, isMove) {
        var me = this;

        if (store.isMoving()) {
            return;
        }

        me.processStore(me.store);
        me.fireEvent('refresh', me);
    },

    onClear: function(store, records, startIndex) {
        var me = this;

        me.processStore(me.store);
        me.fireEvent('clear', me);
    },

    onAdd: function(store, records, startIndex) {
        var me = this;

        me.processStore(me.store);

        me.fireEvent('replace', me, me.indexOf(records[0]), [], records);
    },

    onIdChanged: function(store, rec, oldId, newId) {
        this.data.updateKey(rec, oldId);
    },

    onUpdate: function(store, record, operation, modifiedFieldNames) {
        var me = this,
            groupInfo,
            firstRec, lastRec, items;

        if (me.isGrouped()) {
            groupInfo = record.group = me.groupingFeature.getRecordGroup(record);

            if (!groupInfo) {
                return;
            }
            if (groupInfo.isCollapsed) {
                me.fireEvent('update', me, groupInfo.placeholder);
            }

            else {
                Ext.suspendLayouts();

                items = groupInfo.items;
                firstRec = items[0];
                lastRec = items[items.length - 1];

                if (firstRec !== record) {
                    firstRec.group = groupInfo;
                    me.fireEvent('update', me, firstRec, 'edit', modifiedFieldNames);
                    delete firstRec.group;
                }
                if (lastRec !== record && lastRec !== firstRec && me.groupingFeature.showSummaryRow) {
                    lastRec.group = groupInfo;
                    me.fireEvent('update', me, lastRec, 'edit', modifiedFieldNames);
                    delete lastRec.group;
                }
                Ext.resumeLayouts(true);
            }

            delete record.group;
        }
    },

    onGroupChange: function(store, grouper) {
        if (!grouper) {
            this.processStore(store);
        }
        this.fireEvent('groupchange', store, grouper);
    },

    destroy: function() {
        var me = this;

        me.bindStore(null);
        me.clearListeners();
        Ext.destroyMembers(me, 'data', 'groupingFeature');
    },

    getGroups: function() {
        var me = this,
            sorters = me.store.getSorters(),
            groupers = me.store.groupers,
            i, len, ind, grouper, sorter;
        if (!me.groupCache) {
            me.store.suspendEvents();
            len = groupers ? groupers.length : 0;
            for (i = len-1; i >= 0; i--) {
                grouper = groupers[i];
                ind = sorters.indexOfKey(grouper.getProperty());
                if (ind >= 0) {
                    sorter = sorters.getAt(ind);
                    sorters.remove(sorter);
                } else {
                    sorter = grouper;
                }
                sorters.insert(0, sorter);
            }
            me.store.resumeEvents();
            me.groupCache = me.getGroupsForGrouperIndex(me.store.data, 0).items;
        }
        return me.groupCache;
    },

    getGroupsForGrouperIndex: function(records, grouperIndex) {
        var me = this,
            groupers = me.store.groupers,
            grouper = groupers ? groupers[grouperIndex] : null,
            groups = groupers ? me.getGroupsForGrouper(records, grouper) : [],
            length = groups.length,
            items, group;

        if (groupers && grouperIndex + 1 < groupers.length) {
            for (var i = 0; i < length; i++) {
                group = groups.getAt(i);
                items = me.getGroupsForGrouperIndex(group, grouperIndex + 1);
                group.clear();
                group.withSubGroups = true;
                group.add(items.items);
            }
        }

        for (var i = 0; i < length; i++) {
            group = groups.getAt(i);
            group.depth = grouperIndex;
        }

        return groups;
    },

    getGroupsForGrouper: function(records, grouper) {
        var length = records.length,
            groups = new Ext.util.Collection({rootProperty: 'data',
                extraKeys: {
                    byInternalId: {
                        property: 'internalId',
                        rootProperty: ''
                    }
                }
            }),
            values = [],
            newValue,
            record,
            group,
            ind, Model, modelData, key, groupPlaceholder;

        if (grouper) {
            for (var i = 0; i < length; i++) {
                record = records.getAt(i);
                newValue = grouper.getGroupString(record);
                ind = values.indexOf(newValue);

                if (ind === -1) {
                    group = this.createGroup(records, newValue);
                    group.grouper = grouper;
                    grouper.isCollapsed = true;

                    if (records instanceof Ext.util.Group) {
                        group.parentGroup = records;
                    }

                    key = this.groupingFeature.getGroupKey(group);
                    Model = this.store.getModel();
                    modelData = {};
                    modelData[grouper.getProperty()] = key;
                    group.placeholder = groupPlaceholder = new Model(modelData);
                    groupPlaceholder.isNonData = groupPlaceholder.isCollapsedPlaceholder = true;
                    groupPlaceholder.group = group;

                    groups.add(group);
                    values.push(newValue);
                } else {
                    group = groups.getAt(ind);
                }

                group.add(record);
            }
        }

        return groups;
    },

    createGroup: function(source, key) {
        var group = new Ext.util.Group({
            groupKey: key,
            //rootProperty: this.getItemRoot(),
            rootProperty: 'data'
            //sorters: source.getSorters()
        });
        return group;
    },

    isGrouped: function() {
        return this.store.groupers ? this.store.groupers.length > 0 : false;
    },

    indexOfPlaceholder: function(record) {
        return this.data.indexOf(record);
    },

    contains: function(record){
        return this.data.contains(record);
    }
});