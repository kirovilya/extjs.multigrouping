
Ext.define("BarsUp.grid.MultiSearch", {
	extend: "Ext.container.Container",
	alternateClassName: "BarsUp.grid.MultiSearch",
	alias: 'plugin.gms',
	config:{
		store:null,
		columns:null
	},
	layout:"hbox",
	dock:"top",
	baseCls:"gms-ct",
	buffer:500,
	clearItemIconCls:"icon-clear-filter",
	clearItemT:"Clear Filter",
	filterOnEnter:false,
	height:24,
	iconColumn:true,
	inSeparator:"|",
	operatorRe:/^(=|!=|<=|>=|<|>|in |like )/,
	parseOperator:true,
	weight:1000,
	applyStore:function(b){
		b=b||this.grid.getStore();
		if(!b.getFilters){
			b.getFilters=function(){
				return this.filters
			}
		}
		return b
	},
	updateStore:function(e,g){
		var h=this,f={
			filterchange:{
				scope:this,
				fn:h.onStoreFilterChange
			}
		};
		if(g){
			g.un(f)
		}
		if(e){
			e.on(f);
			h.setValuesFromStore()
		}
	},
	onStoreFilterChange:function(){
		var b=this;
		if(!b.filtering){
			b.setValuesFromStore()
		}
	},
	setValuesFromStore:function(){
		var c=this,d=c.getStoreFilters();
		if(d){
			c.setValues(d,true)
		}else{
			c.clearValues(true)
		}
	},
	getStoreFilters:function(){
		var f=this,d=f.getStore(),e=null;
		if(d){
			d.getFilters().each(
				function(j){
					var a=j.getProperty?j.getProperty():j.property,k=j.getOperator?j.getOperator():j.operator,b=j.getValue?j.getValue():j.value,c="";
					if("in"===k){
						b=b.join(",")
					}
					if(Ext.Array.contains(["in","like"],k)){
						c=" "
					}
					e=e||{};
					e[a]=(k?k+c:"")+b
				}
			)
		}
		return e
	},
	updateColumns:function(){
		var d=this,f=d.headerCt,e=d.grid.getSelectionModel();
		f.suspendEvents();
		if(d.iconColumn){
			d.iconCol=f.add(d.getIconCol())
		}
		d.removeAll(true);
		d.add(d.getFields());
		if("Ext.selection.CheckboxModel"===e.$className){
			d.items.insert(e.injectCheckbox,Ext.widget({
				itemId:"item-"+e.injectCheckbox,
				xtype:"component",
				cls:"gms-nofilter",
				height:d.height
			}))
		}
		f.resumeEvents();
		d.setValuesFromStore();
		d.grid.getView().refresh();
		Ext.Function.defer(function(){
			d.syncCols();
			d.syncUi()
		},1)
	},
	init:function(grid){
		var me=this,g=grid.getView().getHeaderCt(),h=Ext.versions.extjs.major;
		if(Ext.isString(me.operatorRe)){
			me.operatorRe=new RegExp(me.operatorRe.replace(/(^\/|\/$)/g,""))
		}
		me.grid = grid;
		me.headerCt = g;
		me.extVersion = h;

		g.on({
			afterlayout:{
				fn:me.afterHdLayout,
				scope:me
			},
			afterrender:{
				fn:me.afterHdRender,
				scope:me,
				single:true
			}
		});
		grid.on({
			scope:me,
			reconfigure:me.onReconfigure,
            columnmove:me.onColumnMove
		});
		g.on({
			afterrender:{
				fn:me.onAfterRender,
				scope:me,
				single:true
			}
		});
		me.onReconfigure(grid,grid.store,grid.columns);
		grid.getFilter=function(){
			return me
		}
	},
	onReconfigure:function(f,e,d){
		this.setColumns(d);
		this.setStore(e)
	},

	getFields:function(){
		var me = this,
			fields = [],
			columns = me.headerCt.getGridColumns();
		Ext.Array.each(columns, function(col, ind){
			var c = col.filterField || col.filter,
				k = {xtype:"component"},
				a = null;
			if(true===c){
				k.xtype="textfield"
			}else{
				if(c&&c.isComponent){
					k=c
				}else{
					if("string"===typeof c){
						k.xtype=c
					}else{
						if(Ext.isObject(c)){
							Ext.apply(k,c)
						}else{
							k.cls="gms-nofilter";
							k.height=me.height
						}
					}
				}
			}
			if("iconCol" === col.itemId){
				Ext.apply(k, me.getIcon())
			}
			Ext.apply(k,{
				//itemId: col.itemId ? col.itemId : col.dataIndex||"item"+ind,
				itemId: col.dataIndex,
				gridstore: me.grid.getStore()
			});
			a=Ext.widget(k);
			if(me.filterOnEnter){
				a.on("specialkey", me.onSpecialKey, me)
			}else{
				a.on("change", me.onChange, me, {buffer: me.buffer})
			}
			fields.push(a)
		});
		return fields
	},
	onChange:function(c){
		var d=this;
		if(c.isDirty()){
			c.resetOriginalValue();
			d.doFieldChange(c)
		}
	},
	doFieldChange:function(k){
		var me = this,
			l=k.getRawValue(),
			m=k.getItemId(),
			j=me.parseOperator,
			h;
		// если есть стор, значит это комбобокс
		if (k.store) {
			l=k.getValue();
		}
		h = j ? me.parseUserValue(l) : {value:l};
		h.value = k.rawToValue(h.value);
		if (!h.operator && !Ext.isDate(h.value)) {
			h.operator = 'like'
		}
		h.property=m;
		h.id=m;
		me.setFilter(h);
		me.updateClearIcon(k)
	},
	getFilters:function(){
		var c=this,d=[];
		c.items.each(function(a){
			var b;
			if(a.isFormField){
				b=c.getFilterFromField(a);
				if(b){
					d.push(b)
				}
			}
		});
		return d
	},
	getFilterFromField:function(g){
		var me = this,
			value = g.getSubmitValue(),
			parsed;
		if(value){
			parsed = me.parseUserValue(value);
			//parsed.value = g.rawToValue(parsed.value);
			parsed.property=g.getItemId();
			return parsed
		}
		return null
	},
	setFilter:function(d){
		var f=this,e=f.getStore();
		if(Ext.isArray(d)){
			e.clearFilter(0<d.length);
			e.addFilter(d)
		}else{
			f.filtering=true;
			if(!d.value){
				if(4===f.extVersion){
					e.filters.removeAtKey(d.property);
					if(e.filters.getCount()){
						e.filter()
					}else{
						e.clearFilter()
					}
				}else{
					e.removeFilter(d.property)
				}
			}else{
				e.addFilter(d)
			}
			f.filtering=false
		}
	},
	clearField: function(field,d){
		var me=this;
		if (field && Ext.isFunction(field.setValue) && !field.readOnly && !field.disabled){
			if (true===d){
				field.suspendEvents();
			}
			field.setValue("");
			field.resetOriginalValue();
			if (true===d){
				field.resumeEvents();
			}
			if (true!==d){
				me.doFieldChange(field);
			}
		}
	},
	setValues:function(f,h){
		var e=this,g;
		if(f&&Ext.isObject(f)){
			e.clearValues(true);
			Ext.Object.each(f,function(b,a){
				g=e.items.get(b);
				if(g&&Ext.isFunction(g.setValue)){
					if(true===h){
						g.suspendEvents()
					}
					g.setValue(a);
					g.resetOriginalValue();
					if(true===h){
						g.resumeEvents()
					}
				}
			})
		}
	},
	clearValues:function(c){
		var d=this;
		d.items.each(function(a){
			d.clearField(a,c)
		});
		if(!c){
			d.getStore().clearFilter()
		}
	},
	onAfterRender:function(){
		/*var f=this,e,d;
		if(!Ext.isFunction(f.getScrollerEl)){
			f.getScrollerEl=function(){
				return f.layout.innerCt
			}
		}
		e=f.getScrollerEl();
		e.on("scroll",f.onFilterScroll,f)*/
	},
	onFilterScroll:function(){
		var d=this,c=d.getScrollerEl().getScrollLeft();
		if(5===d.extVersion){
			d.grid.getView().scrollTo(c,0)
		}else{
			d.grid.getView().getEl().scrollTo("left",c)
		}
	},
	parseUserValue:function(q){
		var m=this,o=m.operatorRe,p=m.inSeparator,n,j,l,k=Ext.String.trim;
		if(!q){
			return{value:""}
		}
		n=q.split(o);
		if(2>n.length){
			return{value:q}
		}
		l=k(n[2]);
		j=k(n[1]);
		if("in"!==j){
			return{value:l,operator:j}
		}
		n = k(l).split(p);
		n.forEach(function(item, ind, ar){
			ar[ind] = item == "_-_" ? undefined : item;
		});
		return{
			value: n,
			operator:j
		}
	},
	onSpecialKey:function(f,d){
		var e=this;
		if(Ext.EventObject.ENTER===d.getKey()){
			e.setFilter(e.getFilters())
		}
	},
	onIconClick:function(c){
		var d=this;
		if(d.filterMenu){
			d.filterMenu.showBy(c.getTarget("div.x-tool"))
		}
	},
	getIconCol:function(){
		return{
			width:21,
			menuDisabled:true,
			hideable:false,
			sortable:false,
			itemId:"iconCol",
			draggable:false,
			hoverCls:"",
			baseCls:""
		}
	},
	getIcon:function(){
		return{
			autoEl:{
				tag:"div",
				children:[{
					tag:"img",
					src:"data:image/gif;base64,R0lGODlhAQABAID/AMDAwAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==",
					cls:"gms-icon x-tool-img x-tool-gear"
				}]
			},
			cls:"gms-nofilter x-tool",
			overCls:"x-tool-over",
			listeners:{
				click:{
					fn:this.onIconClick,
					scope:this,
					element:"el"
				}
			}
		}
	},
	createFilterMenu:function(){
		var c=this,d=[];
		if(!c.filterMenu){
			d.push({
				text:c.clearItemT,
				iconCls:c.clearItemIconCls,
				scope:c,
				handler:function(){
					c.clearValues(true);
					c.getStore().clearFilter()
				}
			});
			c.filterMenu=Ext.widget("menu",{defaultAlign:"tr-br?",items:d})
		}
	},
	updateClearIcon:function(h){
		var k=this,f="gms-hasvalue",g=h.bodyEl?h.bodyEl.down("input"):null,j=h.getValue?h.getValue():null;
		if(g){
			g=k.extVersion===4?g.up("td"):g.up("div");
			if(false!==h.clearIcon){
				if(!h.clearIcon){
					h.clearIcon=g.createChild({tag:"div",cls:"gms-clear"});
					h.clearIcon.on("click",Ext.bind(k.clearField,k,[h]));
					g.applyStyles({position:"relative"})
				}
				j = Array.isArray(j) && j.length == 0 ? null : j;
				if(j&&!h.readOnly&&!h.disabled){
					g.addCls(f)
				}else{
					g.removeCls(f)
				}
			}
		}
	},
	markFiltered:function(g){
		var me = this,
			h = g.getValue?g.getValue():null,
			f = me.headerCt.getGridColumns()[me.items.indexOf(g)];
		if(!f){
			return
		}
		f=f.getEl();
		f.removeCls("gms-filtered");
		h = Array.isArray(h) && h.length == 0 ? null : h;
		if(h){
			f.addCls("gms-filtered")
		}else{
			f.removeCls("gms-filtered")
		}
	},
	syncUi:function(){
		var b=this;
		b.items.each(function(a){
			if(a&&a.rendered){
				b.updateClearIcon(a);
				b.markFiltered(a)
			}
		})
	},
	syncCols:function(){
		var e=this,d=e.headerCt.getGridColumns(),f;
		if(!e.rendered){
			return
		}
		f=e.headerCt.layout.innerCt.getWidth();
		Ext.Array.each(d,function(c,b){
			var a=e.items.getAt(b);
			if(a){
				a.setWidth(c.getWidth())
			}
		});
		e.layout.targetEl.setWidth(f)
	},
	onGridScroll:function(){
		var d=this,e=d.grid.getView().getEl().getScroll(),f=d.getLayout().innerCt;
		f.scrollTo("left",e.left)
	},
	onColumnMove:function(){
		var b=this;
		b.syncOrder();
		b.grid.getView().refresh();
		b.syncUi();
		b.syncCols();
		Ext.Function.defer(b.onGridScroll,1,b)
	},
	syncOrder:function(){
		var e=this,g=e.headerCt.getGridColumns(),f,h;
		for(f=0;f<g.length;f++){
			h=e.items.get(g[f].dataIndex);
			if(h){
				e.items.insert(f,h)
			}
		}
		//e.doLayout()
	},
	afterHdLayout:function(){
		var b=this;
		if(!b.grid.reconfiguring){
			b.syncCols();
			b.syncUi()
		}
	},
	afterHdRender:function(){
		var c=this,d=c.grid;d.dockedItems.add(c);
		d.getView().on({
			scroll:{fn:c.onGridScroll,scope:c}
		})
		c.createFilterMenu()
	}
});