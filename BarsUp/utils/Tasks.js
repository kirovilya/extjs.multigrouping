Ext.define('BarsUp.utils.Tasks', {
    singleton: true,

    tasks: {},

    start: function (list, process, callback, callbackStop, callbackStep) {
        var config = {
            id: Ext.id(null, 'task-')
        };

        setTimeout(function () {
            var start = +new Date(),
                stop = false,
                task;

            do {
                task = list.shift();

                if (Ext.isFunction(task)) {
                    stop = task(config) === false;
                } else {
                    stop = process(task, config) === false;
                }
            }
            while (list.length > 0 && (+new Date() - start < 100));

            var ln = list.length;

            if (stop) {
                callbackStop && callbackStop(task);
            } else if (ln > 0) {
                callbackStep && callbackStep(task, ln);
                setTimeout(arguments.callee, 10);
            } else {
                callback && callback(config);
            }
        }, 10);

        this.tasks[config.id] = config;

        return config.id;
    },

    getTask: function (id) {
        return this.tasks[id];
    }
});