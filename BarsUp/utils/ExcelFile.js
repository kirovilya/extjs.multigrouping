Ext.define('BarsUp.utils.ExcelFile', {
    alternateClassName: 'BarsUp.ExcelFile',
    extend: 'Ext.util.Observable',

    fontName: 'Tahoma',
    fontSize: 8,

    fileName: 'report',
    mimeType: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',

    numberFormats: [
        'General',
        '0',
        '0.00',
        '#,##0',
        '#,##0.00',
        '0%',
        '0.00%',
        '0.00E+00',
        '# ?/?',
        '# ??/??',
        'mm-dd-yy',
        'd-mmm-yy',
        'd-mmm',
        'mmm-yy',
        'h:mm AM/PM',
        'h:mm:ss AM/PM',
        'h:mm',
        'h:mm:ss',
        'm/d/yy h:mm',
        '#,##0 ;(#,##0)',
        '#,##0 ;[Red](#,##0)',
        '#,##0.00;(#,##0.00)',
        '#,##0.00;[Red](#,##0.00)',
        'mm:ss',
        '[h]:mm:ss',
        'mmss.0',
        '##0.0E+0',
        '@'
    ],

    activeSheet: 0,

    borders: {
        bottom: '000000',
        right: '000000',
        left: '000000',
        top: '000000'
    },

    info: {
        creator: '',
        lastModified: new Date(),
        created: new Date(),
        modified: new Date()
    },

    chars: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',

    build: function () {
        var zip = new JSZip(),
            rels = zip.folder('_rels'),
            props = zip.folder('docProps'),
            xl = zip.folder('xl'),
            theme = xl.folder('theme'),
            sheets = xl.folder('worksheets'),
            tables = xl.folder('tables');

        rels.file('.rels', this.getRelsXml());
        theme.file('theme1.xml', this.getThemeXml());
        props.file('core.xml', this.getPropsXml());

        this._styles = [{}];
        this._rels = [];
        this._borders = [];
        this._worksheets = [];
        this._fonts = [];
        this._fills = [];
        this._props = [];
        this._typesString = '';
        this._sharedStrings = [[], 0];

        this.buildWorksheets(sheets, tables);
        this.buildStyles(xl);

        zip.file('[Content_Types].xml', this.getContentTypesXml());
        props.file('app.xml', this.getAppXml());
        xl.folder('_rels').file('workbook.xml.rels', this.getWorkbookRelsXml());
        xl.file('sharedStrings.xml', this.getSharedStringsXml());
        xl.file('workbook.xml', this.getWorkbookXml());

        return zip;
    },

    download: function (opts) {
        opts = opts || {};

        var zip = this.build(),
            name = opts.fileName || this.fileName + '.xlsx';

        if (typeof Blob == 'undefined') {
            /*var modal = $('<div class="ui-modal"/>').appendTo('body');

            $('<div class="ui-save-to-hard"/>').appendTo(modal).downloadify({
                filename: name,
                data: zip.generate({
                    mimeType: this.mimeType,
                    compression: 'DEFLATE'
                }),
                dataType: 'base64',
                onComplete: function () { modal.remove(); },
                onCancel: function () { modal.remove(); },
                onError: function () { modal.remove(); },
                swf: ui.root + 'downloadify.swf',
                downloadImage: ui.root + 'download.png',
                width: 100,
                height: 30,
                transparent: true,
                append: false
            });*/
        } else {
            var blob = zip.generate({
                type: 'blob',
                mimeType: this.mimeType,
                compression: 'DEFLATE'
            });

            // IE 10
            if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                window.navigator.msSaveOrOpenBlob(blob, name + '.xlsx');
            } else {
                var link = $('<a target="_blank" />').appendTo('body'),
                    dom = link.get(0);

                link.attr('href', URL.createObjectURL(blob)).attr('download', name);

                if (dom.click) {
                    dom.click();
                } else if (document.createEvent) {
                    var event = document.createEvent("MouseEvents");
                    event.initEvent("click", true, true);
                    dom.dispatchEvent(event);
                }

                link.remove();
            }
        }
    },

    buildStyles: function (xl) {
        var styles = this._styles,
            index,
            s,
            bi,
            fi,
            i = 1,
            l = styles.length,
            formats = [],
            str = '';

        while (i < l) {
            s = JSON.parse(styles[i]);

            if (s.formatCode !== 'General') {
                index = this.numberFormats.indexOf(s.formatCode);

                if (index < 0) {
                    index = 164 + formats.length;
                    formats.push('<numFmt formatCode="' + s.formatCode + '" numFmtId="' + index + '"/>');
                }

                s.formatCode = index
            } else {
                s.formatCode = 0
            }

            bi = this.buildBorders(s);
            fi = this.buildFonts(s);

            str += '<xf xfId="0" fillId="' +
                this.buildFills(s) +
                '" borderId="' + bi + '" fontId="' + fi + '" numFmtId="' + s.formatCode + '" ' +
                (s.hAlign || s.vAlign ? 'applyAlignment="1" ' : '') +
                (s.formatCode > 0 ? 'applyNumberFormat="1" ' : '') +
                (bi > 0 ? 'applyBorder="1" ' : '') +
                (fi > 0 ? 'applyFont="1"' : '') +
                '>';

            if (s.hAlign || s.vAlign || s.wrapText) {
                str += '<alignment';

                if (s.hAlign) str += ' horizontal="' + s.hAlign + '"';
                if (s.vAlign) str += ' vertical="' + s.vAlign + '"';
                if (s.wrapText) str += ' wrapText="1"';

                str += '/>';
            }

            str += '</xf>';
            i++;
        }

        xl.file('styles.xml', '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><styleSheet xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" mc:Ignorable="x14ac" xmlns:x14ac="http://schemas.microsoft.com/office/spreadsheetml/2009/9/ac">' +
            (formats.length ? '<numFmts count="' + formats.length + '">' + formats.join('') + '</numFmts>' : '') +

            '<fonts count="' + (this._fonts.length + 1) + '" x14ac:knownFonts="1">' +
            '<font>' +
            '<sz val="' + this.fontSize + '"/>' +
            '<color theme="1"/>' +
            '<name val="' + this.fontName + '"/>' +
            '<family val="2"/>' +
            '<scheme val="minor"/>' +
            '</font>' +
            this._fonts.join('') +
            '</fonts>' +

            '<fills count="2">' +
            '<fill><patternFill patternType="none"/></fill>' +
            '<fill><patternFill patternType="gray125"/></fill>' +
            this._fills.join('') +
            '</fills>' +

            '<borders count="' + (this._borders.length + 1) + '">' +
            '<border><left/><right/><top/><bottom/><diagonal/></border>' +
            this._borders.join('') +
            '</borders>' +

            '<cellStyleXfs count="1"><xf numFmtId="0" fontId="0" fillId="0" borderId="0"/></cellStyleXfs>' +

            '<cellXfs count="' + styles.length + '">' +
            '<xf numFmtId="0" fontId="0" fillId="0" borderId="0" xfId="0"/>'
            + str +
            '</cellXfs>' +
            '<cellStyles count="1"><cellStyle name="Normal" xfId="0" builtinId="0"/></cellStyles>' +
            '<dxfs count="5">' +
            '<dxf>' +
            '<font>' +
            '<b val="0"/>' +
            '<i val="0"/>' +
            '<strike val="0"/>' +
            '<condense val="0"/>' +
            '<extend val="0"/>' +
            '<outline val="0"/>' +
            '<shadow val="0"/>' +
            '<u val="none"/>' +
            '<vertAlign val="baseline"/>' +
            '<sz val="8"/>' +
            '<color rgb="FF000000"/>' +
            '<name val="Tahoma"/>' +
            '<scheme val="none"/>' +
            '</font>' +
            '<fill>' +
            '<patternFill patternType="solid">' +
            '<fgColor indexed="64"/>' +
            '<bgColor rgb="FFD3D3D3"/>' +
            '</patternFill>' +
            '</fill>' +
            '<alignment horizontal="center" vertical="center" textRotation="0" wrapText="1" indent="0" justifyLastLine="0" shrinkToFit="0" readingOrder="0"/>' +
            '<border diagonalUp="0" diagonalDown="0" outline="0">' +
            '<left style="thin">' +
            '<color rgb="FF000000"/>' +
            '</left>' +
            '<right style="thin">' +
            '<color rgb="FF000000"/>' +
            '</right>' +
            '<top/>' +
            '<bottom/>' +
            '</border>' +
            '</dxf>' +
            '<dxf>' +
            '<font>' +
            '<b val="0"/>' +
            '<i val="0"/>' +
            '<strike val="0"/>' +
            '<condense val="0"/>' +
            '<extend val="0"/>' +
            '<outline val="0"/>' +
            '<shadow val="0"/>' +
            '<u val="none"/>' +
            '<vertAlign val="baseline"/>' +
            '<sz val="8"/>' +
            '<color rgb="FFFFFFFF"/>' +
            '<name val="Tahoma"/>' +
            '<scheme val="none"/>' +
            '</font>' +
            '<fill>' +
            '<patternFill patternType="solid">' +
            '<fgColor indexed="64"/>' +
            '<bgColor rgb="FF99CC00"/>' +
            '</patternFill>' +
            '</fill>' +
            '<alignment horizontal="general" vertical="top" textRotation="0" wrapText="1" indent="0" justifyLastLine="0" shrinkToFit="0" readingOrder="0"/>' +
            '<border diagonalUp="0" diagonalDown="0">' +
            '<left/>' +
            '<right style="thin">' +
            '<color rgb="FF000000"/>' +
            '</right>' +
            '<top style="thin">' +
            '<color rgb="FF000000"/>' +
            '</top>' +
            '<bottom style="thin">' +
            '<color rgb="FF000000"/>' +
            '</bottom>' +
            '<vertical/>' +
            '<horizontal/>' +
            '</border>' +
            '</dxf>' +
            '<dxf>' +
            '<border outline="0">' +
            '<top style="thin">' +
            '<color rgb="FF000000"/>' +
            '</top>' +
            '</border>' +
            '</dxf>' +
            '<dxf>' +
            '<border outline="0">' +
            '<bottom style="thin">' +
            '<color rgb="FF000000"/>' +
            '</bottom>' +
            '</border>' +
            '</dxf>' +
            '<dxf>' +
            '<border outline="0">' +
            '<left style="thin">' +
            '<color rgb="FF000000"/>' +
            '</left>' +
            '<right style="thin">' +
            '<color rgb="FF000000"/>' +
            '</right>' +
            '<top style="thin">' +
            '<color rgb="FF000000"/>' +
            '</top>' +
            '<bottom style="thin">' +
            '<color rgb="FF000000"/>' +
            '</bottom>' +
            '</border>' +
            '</dxf>' +
            '</dxfs>' +
            '<tableStyles count="0" defaultTableStyle="TableStyleMedium2" defaultPivotStyle="PivotStyleLight16"/>' +
            '<extLst><ext uri="{EB79DEF2-80B8-43e5-95BD-54CBDDF9020C}" xmlns:x14="http://schemas.microsoft.com/office/spreadsheetml/2009/9/main">' +
            '<x14:slicerStyles defaultSlicerStyle="SlicerStyleLight1"/></ext></extLst></styleSheet>'
        );

        delete this._fonts;
        delete this._borders;
        delete this._fills;
        delete this._styles;
    },

    buildFills: function (style) {
        var index = 0;

        if (style.bgColor) {
            var fill = '<fill><patternFill patternType="solid"><fgColor rgb="FF' + style.bgColor + '"/></patternFill></fill>';
            index = this._fills.indexOf(fill);

            if (index < 0) {
                index = this._fills.push(fill) + 1;
            } else {
                index += 2;
            }
        }

        return index;
    },

    buildBorders: function (style) {
        var index = 0;

        if (style.borders) {
            var borders = ['left', 'right', 'top', 'bottom'],
                border = '<border>',
                color,
                j;

            for (j = 0; j < 4; j++) {
                color = style.borders[borders[j]];

                if (color) {
                    border += '<' + borders[j] + ' style="thin">' + '<color rgb="' + color + '"/></' + borders[j] + '>';
                } else {
                    border += '<' + borders[j] + '/>';
                }
            }

            border += '</border>';
            index = this._borders.indexOf(border);

            if (index < 0) {
                index = this._borders.push(border);
            } else {
                index += 1;
            }
        }

        return index;
    },

    buildFonts: function (style) {
        var index = 0;

        if (style.bold || style.italic || style.fontSize || style.fontName || style.color) {
            var font = '<font>';

            if (style.bold) font += '<b/>';
            if (style.italic) font += '<i/>';

            font += '<sz val="' + (style.fontSize || this.fontSize) + '"/>' +
                '<color ' + (style.color ? 'rgb="FF' + style.color + '"' : 'theme="1"') + '/>' +
                '<name val="' + (style.fontName || this.fontName) + '"/>' +
                '<family val="2"/>' +
                '</font>';

            index = this._fonts.indexOf(font) + 1;

            if (index == 0) {
                index = this._fonts.push(font);
            }
        }

        return index;
    },

    buildWorksheets: function (sheets, tables) {
        var i = this.worksheets.length,
            id,
            sheetRels,
            w,
            name;

        while (i--) {
            id = i + 1;
            w = this.worksheets[i];
            name = this.escapeXML(w.name) || 'Sheet' + id;

            sheets.file('sheet' + id + '.xml', this.buildWorksheet(w, id));

            if (w.table === true) {
                sheetRels = sheetRels || sheets.folder('_rels');
                sheetRels.file('sheet' + id + '.xml.rels', '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships"><Relationship Id="rId1" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/table" Target="../tables/table' + id + '.xml"/></Relationships>');
                tables.file('table' + id + '.xml', this.buildTable(w, id));
                this._typesString = '<Override PartName="/xl/tables/table' + id + '.xml" ContentType="application/vnd.openxmlformats-officedocument.spreadsheetml.table+xml"/>' + this._typesString;
            }

            this._typesString = '<Override PartName="/xl/worksheets/sheet' + id + '.xml" ContentType="application/vnd.openxmlformats-officedocument.spreadsheetml.worksheet+xml"/>' + this._typesString;
            this._props.unshift(name);
            this._rels.unshift('<Relationship Id="rId' + id + '" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/worksheet" Target="worksheets/sheet' + id + '.xml"/>');
            this._worksheets.unshift('<sheet name="' + name + '" sheetId="' + id + '" r:id="rId' + id + '"/>');

            this.worksheets[i] = null;
        }
    },

    buildTable: function (worksheet, id) {
        var cells = Object.keys(worksheet.rendered),
            start = worksheet.tableStart || 1,
            last = cells[cells.length - 1],
            range = 'A' + start + ':' + last,
            str = '<table xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main" id="2" name="Таблица2" displayName="Таблица2" ref="' + range + '">'
                + '<autoFilter ref="' + range + '"/>'
                + '<tableColumns count="' + worksheet.max + '">';

        for (var i = 0; i < worksheet.max; i++) {
            str += '<tableColumn id="' + (i + 1) + '" name="' + this._sharedStrings[0][i] + '"/>';

        }

        str += '</tableColumns>'
            + '<tableStyleInfo name="TableStyleMedium2" showFirstColumn="0" showLastColumn="0" showRowStripes="1" showColumnStripes="0"/>'
            + '</table>';

        return str;
    },

    buildWorksheet: function (worksheet, id) {
        var rows = '',
            str,
            cols = '',
            data = worksheet.data,
            l = data.length,
            i = 0;

        worksheet.columns = [];
        worksheet.merges = [];
        worksheet.rendered = {};
        worksheet.merged = {};
        worksheet.ignored = {};
        worksheet.max = 0;
        worksheet.start = worksheet.start || 0;

        if (data.length) {
            Ext.each(data[worksheet.start], function (c) {
                worksheet.max += (c.colSpan || 1);
            });
        }

        while (i < l) {
            if (!data[i] || !data[i].length) {
                data[i] = [];
                data[i].push({ borders: this.borders, fake: true });
            }

            rows += this.buildWorksheetRow(i, data[i], worksheet);
            data[i] = [];
            i++;
        }

        delete worksheet.data;
        data = worksheet.data = null;

        for (i = 0, l = worksheet.columns.length; i < l; i++) {
            cols += '<col min="' + (i + 1) + '" max="' + (i + 1) + '" width="' + ((worksheet.columns[i].width + 16) / 7) + '" customWidth="1"/>';
        }

        str = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' +
            '<worksheet xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" mc:Ignorable="x14ac" xmlns:x14ac="http://schemas.microsoft.com/office/spreadsheetml/2009/9/ac">'
            + '<dimension ref="A1:' + this.num2alpha(worksheet.max - 1) + l + '"/>'
            + '<sheetViews>'
            + '<sheetView ' + (id === this.activeSheet ? 'tabSelected="1" ' : '') + ' workbookViewId="0"/>'
            + '</sheetViews>'
            + '<sheetFormatPr defaultRowHeight="18" x14ac:dyDescent="0.25"/>'
            + '<cols>' + cols + '</cols>'
            + '<sheetData>' + rows + '</sheetData>'
            + this.getWorksheetMerges(worksheet)
            + '<pageMargins left="0.7" right="0.7" top="0.75" bottom="0.75" header="0.3" footer="0.3"/>';

        if (worksheet.table) {
            str += '<tableParts count="1">' +
                   '<tablePart r:id="rId1"/>' +
                    '</tableParts>';
        }

        str += '</worksheet>';

        return str;
    },

    getWorksheetMerges: function (worksheet) {
        var merges = worksheet.merges,
            ln = merges.length,
            i = 0,
            str = '';

        if (ln > 0) {
            str += '<mergeCells count="' + ln + '">';

            for (; i < ln; i++) {
                str += '<mergeCell ref="' + merges[i].join(':') + '"/>';
            }

            str += '</mergeCells>';
        }

        return str;
    },

    buildWorksheetRow: function (i, data, worksheet) {
        var j = 0,
            cell,
            key,
            st,
            cc,
            rowStr = function (k, c) {
                return '<c r="' + k + '"' + (c.style ? ' s="' + c.style + '"' : '') + (c.t ? ' t="' + c.t + '"' : '') + '/>'
            },
            columns = worksheet.columns,
            merges = worksheet.merges,
            rendered = worksheet.rendered,
            ignored = worksheet.ignored,
            merged = worksheet.merged,
            str = 'r="' + (i + 1) + '" x14ac:dyDescent="0.25">',
            l = data.length,
            height = 0,
            last = l - 1;

        while (j < l) {
            cell = this.formatCellValue(data[j]);
            key = this.num2alpha(j) + (i + 1);
            st = j;

            if (data[j].height > height) {
                height = data[j].height;
            }

            while (rendered[key] || merged[key]) {
                if (!ignored[key] && !rendered[key])
                    str += rowStr(key, cell);

                st++;
                ignored[key] = 1;
                key = this.num2alpha(st) + (i + 1);
            }

            if (!cell.fake) {
                str += '<c r="' + key + '"' + (cell.style ? ' s="' + cell.style + '"' : '') + (cell.t ? ' t="' + cell.t + '"' : '');

                if (cell.value != null) {
                    str += '>' + (cell.formula ? '<f>' + cell.formula + '</f>' : '') + '<v>' + cell.value + '</v></c>';
                } else {
                    str += '/>';
                }

                rendered[key] = 1;

                if (cell.width) {
                    if (!columns[st]) columns[st] = {};
                    columns[st].width = cell.width;
                }
            }

            if (cell.rowSpan > 1 || cell.colSpan > 1) {
                var cs = cell.colSpan || 1,
                    rs = cell.rowSpan || 1,
                    ci = 0,
                    ri = 1,
                    sc = this.num2alpha(st) + (i + 1),
                    ec = this.num2alpha(st + cs - 1) + (i + rs);

                merges.push([sc, ec]);

                while (ri <= rs) {
                    ci = 0;
                    while (ci < cs) {
                        merged[this.num2alpha(st + ci) + (i + ri)] = 1;
                        ci++;
                    }
                    ri++;
                }

                ignored[key] = 1;
            }

            if (i > 0 && j == last) {
                while (st < worksheet.max) {
                    if (!ignored[key] && !rendered[key])
                        str += rowStr(key, cell);

                    st++;
                    ignored[key] = 1;
                    key = this.num2alpha(st) + (i + 1);
                }
            }

            j++;
        }

        return (height ? '<row ht="' + (height * 0.75) + '" customHeight="1" ' : '<row ') + str + '</row>';
    },
    formatCellValue: function (data) {
        var val = data.hasOwnProperty('value') ? data.value : data,
            cell = {
                t: '',
                value: val,
                rowSpan: data.rowSpan,
                colSpan: data.colSpan,
                formula: data.formula,
                fake: data.fake,
                width: data.width
            },
            index,
            style = {
                wrapText: data.wrapText,
                borders: data.borders,
                hAlign: data.hAlign,
                vAlign: data.vAlign,
                bgColor: data.bgColor,
                color: data.color,
                bold: data.bold,
                italic: data.italic,
                fontName: data.fontName,
                fontSize: data.fontSize,
                formatCode: data.formatCode || 'General'
            };


        if (val && typeof val === 'string' && !data.number) {
            val = this.escapeXML($('<div/>').html(val).text());

            index = this._sharedStrings[0].indexOf(val);

            this._sharedStrings[1]++;

            if (index < 0) {
                index = this._sharedStrings[0].push(val) - 1;
            }

            cell.value = index;
            cell.t = 's';
        } else if (typeof val === 'boolean') {
            cell.value = (val ? 1 : 0);
            cell.t = 'b';
        } else if (this.typeOf(val) === 'date') {
            // TODO
            //cell.value = this.convertDate(val);
            style.formatCode = data.formatCode || 'mm-dd-yy';
        } else if (typeof val === 'object') {
            val = null
        }

        style = JSON.stringify(style);
        index = this._styles.indexOf(style);

        if (index < 0) {
            cell.style = this._styles.push(style) - 1;
        } else {
            cell.style = index;
        }

        data = null;

        return cell;
    },

    num2alpha: function (num) {
        var t = Math.floor(num / 26) - 1;

        return (t > -1 ? this.num2alpha(t) : '') + this.chars.charAt(num % 26);
    },

    typeOf: function (obj) {
        return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase();
    },

    escapeXML: function (str) {
        return (str || '')
            .replace(/&/g, '&amp;')
            .replace(/"/g, '&quot;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;')
            .replace(/'/g, '&#x27;');
    },

    getSharedStringsXml: function () {
        return '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><sst xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main" count="'
            + this._sharedStrings[1] + '" uniqueCount="' + this._sharedStrings[0].length + '"><si><t>' + this._sharedStrings[0].join('</t></si><si><t>') + '</t></si></sst>'
    },

    getWorkbookXml: function () {
        return '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><workbook xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships">'
            + '<fileVersion appName="xl" lastEdited="5" lowestEdited="5" rupBuild="9303"/><workbookPr defaultThemeVersion="124226"/><bookViews><workbookView '
            + (this.activeSheet ? 'activeTab="' + this.activeSheet + '" ' : '') + 'xWindow="480" yWindow="60" windowWidth="28195" windowHeight="8505"/></bookViews><sheets>'
            + this._worksheets.join('') + '</sheets><calcPr calcId="145621"/></workbook>';
    },

    getWorkbookRelsXml: function () {
        var rels = this._rels;

        return '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">' +
            rels.join('') +
            '<Relationship Id="rId' + (rels.length + 1) + '" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/sharedStrings" Target="sharedStrings.xml"/>' +
            '<Relationship Id="rId' + (rels.length + 2) + '" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles" Target="styles.xml"/>' +
            '<Relationship Id="rId' + (rels.length + 3) + '" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/theme" Target="theme/theme1.xml"/></Relationships>'
    },

    getAppXml: function () {
        return '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Properties xmlns="http://schemas.openxmlformats.org/officeDocument/2006/extended-properties" xmlns:vt="http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes"><Application>XLSX.js</Application><DocSecurity>0</DocSecurity><ScaleCrop>false</ScaleCrop><HeadingPairs><vt:vector size="2" baseType="variant"><vt:variant><vt:lpstr>Worksheets</vt:lpstr></vt:variant><vt:variant><vt:i4>'
            + this.worksheets.length + '</vt:i4></vt:variant></vt:vector></HeadingPairs><TitlesOfParts><vt:vector size="' + this._props.length + '" baseType="lpstr"><vt:lpstr>' + this._props.join('</vt:lpstr><vt:lpstr>')
            + '</vt:lpstr></vt:vector></TitlesOfParts><Manager></Manager><Company>Microsoft Corporation</Company><LinksUpToDate>false</LinksUpToDate><SharedDoc>false</SharedDoc><HyperlinksChanged>false</HyperlinksChanged><AppVersion>1.0</AppVersion></Properties>'
    },

    getContentTypesXml: function () {
        return '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' +
            '<Types xmlns="http://schemas.openxmlformats.org/package/2006/content-types">' +
            '<Default Extension="rels" ContentType="application/vnd.openxmlformats-package.relationships+xml"/>' +
            '<Default Extension="xml" ContentType="application/xml"/>' +
            '<Override PartName="/xl/workbook.xml" ContentType="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml"/>' +
            this._typesString +
            '<Override PartName="/xl/theme/theme1.xml" ContentType="application/vnd.openxmlformats-officedocument.theme+xml"/>' +
            '<Override PartName="/xl/styles.xml" ContentType="application/vnd.openxmlformats-officedocument.spreadsheetml.styles+xml"/>' +
            '<Override PartName="/xl/sharedStrings.xml" ContentType="application/vnd.openxmlformats-officedocument.spreadsheetml.sharedStrings+xml"/>' +
            '<Override PartName="/docProps/core.xml" ContentType="application/vnd.openxmlformats-package.core-properties+xml"/>' +
            '<Override PartName="/docProps/app.xml" ContentType="application/vnd.openxmlformats-officedocument.extended-properties+xml"/>' +
            '</Types>';
    },

    getPropsXml: function () {
        return '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' +
            '<cp:coreProperties xmlns:cp="http://schemas.openxmlformats.org/package/2006/metadata/core-properties" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:dcmitype="http://purl.org/dc/dcmitype/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">' +
            '<dc:creator>' + this.info.creator + '</dc:creator>' +
            '<cp:lastModifiedBy>' + this.info.lastModified + '</cp:lastModifiedBy>' +
            '<dcterms:created xsi:type="dcterms:W3CDTF">' + this.info.created.toISOString() + '</dcterms:created>' +
            '<dcterms:modified xsi:type="dcterms:W3CDTF">' + this.info.modified.toISOString() + '</dcterms:modified>' +
            '</cp:coreProperties>';
    },

    getRelsXml: function () {
        return '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' +
            '<Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">' +
            '<Relationship Id="rId3" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/extended-properties" Target="docProps/app.xml"/>' +
            '<Relationship Id="rId2" Type="http://schemas.openxmlformats.org/package/2006/relationships/metadata/core-properties" Target="docProps/core.xml"/>' +
            '<Relationship Id="rId1" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument" Target="xl/workbook.xml"/>' +
            '</Relationships>';
    },

    getThemeXml: function () {
        return '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' +
            '<a:theme xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" name="Office Theme">' +
            '<a:themeElements>' +
            '<a:clrScheme name="Office">' +
            '<a:dk1><a:sysClr val="windowText" lastClr="000000"/></a:dk1>' +
            '<a:lt1><a:sysClr val="window" lastClr="FFFFFF"/></a:lt1>' +
            '<a:dk2><a:srgbClr val="1F497D"/></a:dk2>' +
            '<a:lt2><a:srgbClr val="EEECE1"/></a:lt2>' +
            '<a:accent1><a:srgbClr val="4F81BD"/></a:accent1>' +
            '<a:accent2><a:srgbClr val="C0504D"/></a:accent2>' +
            '<a:accent3><a:srgbClr val="9BBB59"/></a:accent3>' +
            '<a:accent4><a:srgbClr val="8064A2"/></a:accent4>' +
            '<a:accent5><a:srgbClr val="4BACC6"/></a:accent5>' +
            '<a:accent6><a:srgbClr val="F79646"/></a:accent6>' +
            '<a:hlink><a:srgbClr val="0000FF"/></a:hlink>' +
            '<a:folHlink><a:srgbClr val="800080"/></a:folHlink>' +
            '</a:clrScheme>' +
            '<a:fontScheme name="Office">' +
            '<a:majorFont>' +
            '<a:latin typeface="Cambria"/>' +
            '<a:ea typeface=""/>' +
            '<a:cs typeface=""/>' +
            '<a:font script="Jpan" typeface="MS P????"/>' +
            '<a:font script="Hang" typeface="?? ??"/>' +
            '<a:font script="Hans" typeface="??"/>' +
            '<a:font script="Hant" typeface="????"/>' +
            '<a:font script="Arab" typeface="Times New Roman"/>' +
            '<a:font script="Hebr" typeface="Times New Roman"/>' +
            '<a:font script="Thai" typeface="Tahoma"/>' +
            '<a:font script="Ethi" typeface="Nyala"/>' +
            '<a:font script="Beng" typeface="Vrinda"/>' +
            '<a:font script="Gujr" typeface="Shruti"/>' +
            '<a:font script="Khmr" typeface="MoolBoran"/>' +
            '<a:font script="Knda" typeface="Tunga"/>' +
            '<a:font script="Guru" typeface="Raavi"/>' +
            '<a:font script="Cans" typeface="Euphemia"/>' +
            '<a:font script="Cher" typeface="Plantagenet Cherokee"/>' +
            '<a:font script="Yiii" typeface="Microsoft Yi Baiti"/>' +
            '<a:font script="Tibt" typeface="Microsoft Himalaya"/>' +
            '<a:font script="Thaa" typeface="MV Boli"/>' +
            '<a:font script="Deva" typeface="Mangal"/>' +
            '<a:font script="Telu" typeface="Gautami"/>' +
            '<a:font script="Taml" typeface="Latha"/>' +
            '<a:font script="Syrc" typeface="Estrangelo Edessa"/>' +
            '<a:font script="Orya" typeface="Kalinga"/>' +
            '<a:font script="Mlym" typeface="Kartika"/>' +
            '<a:font script="Laoo" typeface="DokChampa"/>' +
            '<a:font script="Sinh" typeface="Iskoola Pota"/>' +
            '<a:font script="Mong" typeface="Mongolian Baiti"/>' +
            '<a:font script="Viet" typeface="Times New Roman"/>' +
            '<a:font script="Uigh" typeface="Microsoft Uighur"/>' +
            '<a:font script="Geor" typeface="Sylfaen"/>' +
            '</a:majorFont>' +
            '<a:minorFont>' +
            '<a:latin typeface="Calibri"/>' +
            '<a:ea typeface=""/>' +
            '<a:cs typeface=""/>' +
            '<a:font script="Jpan" typeface="MS P????"/>' +
            '<a:font script="Hang" typeface="?? ??"/>' +
            '<a:font script="Hans" typeface="??"/>' +
            '<a:font script="Hant" typeface="????"/>' +
            '<a:font script="Arab" typeface="Arial"/>' +
            '<a:font script="Hebr" typeface="Arial"/>' +
            '<a:font script="Thai" typeface="Tahoma"/>' +
            '<a:font script="Ethi" typeface="Nyala"/>' +
            '<a:font script="Beng" typeface="Vrinda"/>' +
            '<a:font script="Gujr" typeface="Shruti"/>' +
            '<a:font script="Khmr" typeface="DaunPenh"/>' +
            '<a:font script="Knda" typeface="Tunga"/>' +
            '<a:font script="Guru" typeface="Raavi"/>' +
            '<a:font script="Cans" typeface="Euphemia"/>' +
            '<a:font script="Cher" typeface="Plantagenet Cherokee"/>' +
            '<a:font script="Yiii" typeface="Microsoft Yi Baiti"/>' +
            '<a:font script="Tibt" typeface="Microsoft Himalaya"/>' +
            '<a:font script="Thaa" typeface="MV Boli"/>' +
            '<a:font script="Deva" typeface="Mangal"/>' +
            '<a:font script="Telu" typeface="Gautami"/>' +
            '<a:font script="Taml" typeface="Latha"/>' +
            '<a:font script="Syrc" typeface="Estrangelo Edessa"/>' +
            '<a:font script="Orya" typeface="Kalinga"/>' +
            '<a:font script="Mlym" typeface="Kartika"/>' +
            '<a:font script="Laoo" typeface="DokChampa"/>' +
            '<a:font script="Sinh" typeface="Iskoola Pota"/>' +
            '<a:font script="Mong" typeface="Mongolian Baiti"/>' +
            '<a:font script="Viet" typeface="Arial"/>' +
            '<a:font script="Uigh" typeface="Microsoft Uighur"/>' +
            '<a:font script="Geor" typeface="Sylfaen"/>' +
            '</a:minorFont>' +
            '</a:fontScheme>' +
            '<a:fmtScheme name="Office">' +
            '<a:fillStyleLst>' +
            '<a:solidFill><a:schemeClr val="phClr"/></a:solidFill>' +
            '<a:gradFill rotWithShape="1">' +
            '<a:gsLst>' +
            '<a:gs pos="0"><a:schemeClr val="phClr"><a:tint val="50000"/><a:satMod val="300000"/></a:schemeClr></a:gs>' +
            '<a:gs pos="35000"><a:schemeClr val="phClr"><a:tint val="37000"/><a:satMod val="300000"/></a:schemeClr></a:gs>' +
            '<a:gs pos="100000"><a:schemeClr val="phClr"><a:tint val="15000"/><a:satMod val="350000"/></a:schemeClr></a:gs>' +
            '</a:gsLst>' +
            '<a:lin ang="16200000" scaled="1"/>' +
            '</a:gradFill>' +
            '<a:gradFill rotWithShape="1">' +
            '<a:gsLst>' +
            '<a:gs pos="0"><a:schemeClr val="phClr"><a:shade val="51000"/><a:satMod val="130000"/></a:schemeClr></a:gs>' +
            '<a:gs pos="80000"><a:schemeClr val="phClr"><a:shade val="93000"/><a:satMod val="130000"/></a:schemeClr></a:gs>' +
            '<a:gs pos="100000"><a:schemeClr val="phClr"><a:shade val="94000"/><a:satMod val="135000"/></a:schemeClr></a:gs>' +
            '</a:gsLst>' +
            '<a:lin ang="16200000" scaled="0"/>' +
            '</a:gradFill>' +
            '</a:fillStyleLst>' +
            '<a:lnStyleLst>' +
            '<a:ln w="9525" cap="flat" cmpd="sng" algn="ctr"><a:solidFill><a:schemeClr val="phClr"><a:shade val="95000"/><a:satMod val="105000"/></a:schemeClr></a:solidFill><a:prstDash val="solid"/></a:ln>' +
            '<a:ln w="25400" cap="flat" cmpd="sng" algn="ctr"><a:solidFill><a:schemeClr val="phClr"/></a:solidFill><a:prstDash val="solid"/></a:ln>' +
            '<a:ln w="38100" cap="flat" cmpd="sng" algn="ctr"><a:solidFill><a:schemeClr val="phClr"/></a:solidFill><a:prstDash val="solid"/></a:ln>' +
            '</a:lnStyleLst>' +
            '<a:effectStyleLst>' +
            '<a:effectStyle><a:effectLst><a:outerShdw blurRad="40000" dist="20000" dir="5400000" rotWithShape="0"><a:srgbClr val="000000"><a:alpha val="38000"/></a:srgbClr></a:outerShdw></a:effectLst></a:effectStyle>' +
            '<a:effectStyle><a:effectLst><a:outerShdw blurRad="40000" dist="23000" dir="5400000" rotWithShape="0"><a:srgbClr val="000000"><a:alpha val="35000"/></a:srgbClr></a:outerShdw></a:effectLst></a:effectStyle>' +
            '<a:effectStyle><a:effectLst><a:outerShdw blurRad="40000" dist="23000" dir="5400000" rotWithShape="0"><a:srgbClr val="000000"><a:alpha val="35000"/></a:srgbClr></a:outerShdw></a:effectLst><a:scene3d><a:camera prst="orthographicFront"><a:rot lat="0" lon="0" rev="0"/></a:camera><a:lightRig rig="threePt" dir="t"><a:rot lat="0" lon="0" rev="1200000"/></a:lightRig></a:scene3d><a:sp3d><a:bevelT w="63500" h="25400"/></a:sp3d></a:effectStyle>' +
            '</a:effectStyleLst>' +
            '<a:bgFillStyleLst>' +
            '<a:solidFill><a:schemeClr val="phClr"/></a:solidFill>' +
            '<a:gradFill rotWithShape="1"><a:gsLst><a:gs pos="0"><a:schemeClr val="phClr"><a:tint val="40000"/><a:satMod val="350000"/></a:schemeClr></a:gs><a:gs pos="40000"><a:schemeClr val="phClr"><a:tint val="45000"/><a:shade val="99000"/><a:satMod val="350000"/></a:schemeClr></a:gs><a:gs pos="100000"><a:schemeClr val="phClr"><a:shade val="20000"/><a:satMod val="255000"/></a:schemeClr></a:gs></a:gsLst><a:path path="circle"><a:fillToRect l="50000" t="-80000" r="50000" b="180000"/></a:path></a:gradFill>' +
            '<a:gradFill rotWithShape="1"><a:gsLst><a:gs pos="0"><a:schemeClr val="phClr"><a:tint val="80000"/><a:satMod val="300000"/></a:schemeClr></a:gs><a:gs pos="100000"><a:schemeClr val="phClr"><a:shade val="30000"/><a:satMod val="200000"/></a:schemeClr></a:gs></a:gsLst><a:path path="circle"><a:fillToRect l="50000" t="50000" r="50000" b="50000"/></a:path></a:gradFill>' +
            '</a:bgFillStyleLst>' +
            '</a:fmtScheme>' +
            '</a:themeElements>' +
            '<a:objectDefaults/>' +
            '<a:extraClrSchemeLst/>' +
            '</a:theme>';
    }
});