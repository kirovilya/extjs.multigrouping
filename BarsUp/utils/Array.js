Ext.define('BarsUp.utils.Array', {
    alternateClassName: 'BarsUp.Array',

    /**
     * Конструктор
     * @constructor
     * @param {Array} arr Массив объектов
     */
    constructor: function(arr){
        this.list = arr || [];
    },

    /**
     * Добавить объект
     * @param {Object} obj Объект
     * @param {Number} [index] Индекс объекта
     * @returns {Object} Добавленный объект
     */
    add: function(obj, index){
        if(Ext.isEmpty(index)){
            this.list.push(obj);
        } else {
            this.list.splice(index, 0, obj);
        }

        return obj;
    },

    /**
     * Удалить объект
     * @param {Object} obj Объект
     */
    remove: function(obj){
        var l = this.list,
            i = l.length;

        while (i--) {
            if (l[i] === obj) {
                l.splice(i, 1);
                return;
            }
        }
    },

    /**
     * Список содержит объект
     * @param {Object} obj Объект
     * @returns {Number/Boolean} Индекс объекта в списке
     */
    contains: function(obj){
        var l = this.list,
            i = l.length;

        while (i--) {
            if (l[i] === obj) return i;
        }

        return false;
    },

    /**
     * Итерация списка объектов
     * @param {Function} callback Метод итерации
     * @param {Object} [scope] Область видимости
     */
    each: function(callback, scope){
        scope = scope || this;

        var i = 0,
            l = this.list,
            m = l.length;

        for(;i<m;i+=1){
            if (callback.call(scope, l[i], i) === false) {
                return;
            }
        }
    },

    /**
     * Сгруппировать список объектов
     * @param {String/String[]} name Список свойств для группировки
     * @param {Array} sorters Список описаний сортировок
     * @param {Boolean} [append] Формировать конечные записи
     * @returns {Array} Список сруппированных объектов
     */
    groupBy: function(name, sorters, append){
        var result = [],
            sorter = sorters ? sorters[name] : null,
            tmp = {},
            allowAppend = append !== false || (append === false && arguments[3] !== true);

        if(Ext.isArray(name)){
            var allNames = arguments[4] || name.slice();
            var data = this.groupBy(name[0], sorters, append, name.length <= 1, allNames);

            name.shift();

            this.groupByArray(name, data, sorters, append, allNames);

            return data;
        }

        this.each(function(item){
            var key = item[name] || '',
                group = tmp[key];

            if(!group){
                group = {
                    group: key,
                    code: name,
                    items: []
                };

                result.push(group);
                tmp[key]= group;
            }

            if(allowAppend){
                group.items.push(item);
            }
        });

        if(sorter){
            result.sort(function(el1, el2){
                var v1 = el1.group.toLowerCase(),
                    v2 = el2.group.toLowerCase();

                return v1 > v2 ? 1 : (v1 < v2 ? -1 : 0);
            });

            if(sorter == 'DESC'){
                result.reverse();
            }
        }

        var all = arguments[4] || [],
            sortFn;

        if(all.indexOf(name) == all.length - 1){
            Ext.iterate(sorters, function(k,v){
                if(all.indexOf(k) < 0){
                    var opt = v == 'DESC' ? -1 : 0;

                    if(sortFn){
                        sortFn = sortFn.thenBy(k, opt);
                    } else {
                        sortFn = firstBy(k, opt);
                    }
                }
            });

            if(sortFn){
                Ext.each(result, function(x){
                    x.items.sort(sortFn);
                });
            }
        }

        return result;
    },

    /** @private */
    groupByArray: function(groups, data, sorters, append, allNames){
        if(groups.length == 0){
            return;
        }

        var g = groups[0];
        groups.shift();

        Ext.each(data, function(item){
            item.items = new BarsUp.Array(item.items).groupBy(g, sorters, append, groups.length == 0, allNames);
            this.groupByArray(groups.slice(0), item.items, sorters, append, allNames);
            if(groups.length == 0){

            }
        }, this);
    },

    /**
     * Сформировать список значений объектов
     * @param {String/Function} query Имя свойства или функция
     * @returns {Array} Список значений
     */
    map: function(query){
        var result = [],
            queryFn = query;

        if(Ext.isString(query)){
            queryFn = function(item){
                return item[query];
            };
        }

        this.each(function(item){
            result.push(queryFn(item));
        });

        return result;
    },

    /**
     * Функция поиска по маске условий
     * @param {Object} item Объект
     * @param {Object} query Маска условий
     * @returns {boolean} Результат выполнения
     */
    queryFn: function(item, query){
        var r = true, i, val, q;

        for (i in query) {
            q = query[i];
            val = item[i];

            if(val === null || val === undefined || val === ''){
                val = null;
            }

            r = q instanceof Array
                ? q.indexOf(val) >= 0
                : val == q;

            if (!r) {
                return r;
            }
        }

        return r;
    },

    /**
     * Поиск объектов по условиям
     * @param {Function/Object} query Метод/Маска условий поиска
     * @param {Function} [callback] Метод обработки найденного объекта
     * @param {Object} [scope] Область видимости
     * @returns {Array} Массив найденных объектов
     */
    find: function(query, callback, scope){
        var me = this,
            result = [],
            queryFn = Ext.isFunction(query) ? query : me.queryFn,
            filter = function(item){
                if(item !== undefined && item !== null && queryFn(item, query)) {
                    if(callback) {
                        return callback.apply(scope, [item]);
                    }
                    return true;
                }
                return false;
            };

        this.each(function(item){
            var r = filter(item);

            if(r !== false){
                result.push(item);

                if(r == 'stop'){
                    return false;
                }
            }
        });

        return result;
    },

    /**
     * Поиск объект по условиям
     * @param {Function/Object} query Метод/Маска условий поиска
     * @returns {Object} Найденный объект
     */
    findOne: function(query){
        var result = null;

        this.find(query, function(item){
            result = item;
            return 'stop';
        });

        return result;
    },

    findAndGroup: function(query, group, sorters){
        var res = new BarsUp.Array(this.find(query));

        if(group && group.length){
            return res.groupBy(group, sorters);
        }

        var sortFn;

        Ext.iterate(sorters, function(k,v){
            var opt = v == 'DESC' ? -1 : 0;

            if(sortFn){
                sortFn = sortFn.thenBy(k, opt);
            } else {
                sortFn = firstBy(k, opt);
            }
        });

        if(sortFn){
            res.list.sort(sortFn);
        }

        /*res.list.sort(function(xx1, xx2){
            var x1 = xx1['@isNewRecord'],
                x2 = xx2['@isNewRecord'];

            x1 = x1 >= 0 ? x1 : -1;
            x2 = x2 >= 0 ? x2 : -1;

            return x1 - x2;
        });*/

        return res.list;
    },

    /***
     * Получить объект по индексу
     * @param index
     * @returns {Object} Объект
     */
    get: function(index){
        return this.list[index];
    },

    /**
     * Получить список значений по свойству
     * @param {String} name Имя свойства
     * @param {Function} [callbackFn]
     * @returns {Array} Список значений
     */
    getGroups: function(name, callbackFn){
        var groups = [],
            callback = callbackFn || function(item){
                if(groups.indexOf(item[name]) < 0) {
                    groups.push(item[name]);
                }
            };

        this.each(function(item){
            callback(item, name, groups);
        });

        return groups;
    }
});


var firstBy = (function() {
    function makeCompareFunction(f, direction){
        if(typeof(f)!="function"){
            var prop = f;
            f = function(v1,v2){return v1[prop] < v2[prop] ? -1 : (v1[prop] > v2[prop] ? 1 : 0);}
        }
        if(f.length === 1) {
            // f is a unary function mapping a single item to its sort score
            var uf = f;
            f = function(v1,v2) {return uf(v1) < uf(v2) ? -1 : (uf(v1) > uf(v2) ? 1 : 0);}
        }
        if(direction === -1)return function(v1,v2){return -f(v1,v2)};
        return f;
    }
    /* mixin for the `thenBy` property */
    function extend(f, d) {
        f=makeCompareFunction(f, d);
        f.thenBy = tb;
        return f;
    }

    /* adds a secondary compare function to the target function (`this` context)
     which is applied in case the first one returns 0 (equal)
     returns a new compare function, which has a `thenBy` method as well */
    function tb(y, d) {
        var x = this;
        y = makeCompareFunction(y, d);
        return extend(function(a, b) {
            return x(a,b) || y(a,b);
        });
    }
    return extend;
})();